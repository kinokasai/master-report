# Conclusion & Future work

The demo application has never been released.
Try Michelson has successfully been used both as
a working and training tool. Nomadic Labs
provides training sessions regularly. Try
Michelson has been used during these workshops
and has proven especially popular. It is
available online at [](https://try-michelson.tzalpha.net)

### Server

The server could incorporate the "smart parts"
currently present in Try Michelson, and expose
an API corresponding to Language Server Protocol
[@lsp]. This would allow the server to be used
in conjunction with many modern editors
supporting lsp. Experimentation has been started
in this direction.

### Client

Besides adapting to the server's lsp API, Try
Michelson could go undergo an interface change.
The space given to the editor is in fact rather
small proportionally to its usage.

Client code must be made more modular to provide
standalone components that can be reused in
other applications.

Future work for Try Michelson also includes the
integration of _Mi-cho-coq_, a coq
framework for proving smart contracts.

The goal is to provide the final user a complete
environment to write, simulate, prove, and
deploy smart contracts on the Tezos blockchain.

### Apps

The framework could be enriched with other
applications, demoing other kinds of smart
contract.

## Code and artifacts

All the code is available under the MIT license.

* Try Michelson: `gitlab.com/nomadic-labs/try-michelson.git`
* Server: `gitlab.com/nomadic-labs/tezos-lang-server.git`
* Application: `gitlab.com/kinokasai/tezos-dapps.git`
* Visual Studio Code extension: `gitlab.com/kinokasai/vscode-michelson.git`