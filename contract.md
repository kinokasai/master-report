## Contract Component

The Contact component is an example of a
_phantom_ component. In its bubble, this should
be a reducerComponent. After all, its state
changes - when the user types, for example.
However, it does not hold any state. The reason
for this is that what would be its own state is
needed outside of this component.

The website needs access to the code for
typechecking, to the contract as a whole, to
send for tracing. There's no way in react to
query a component for its state. Instead, the
mantra is _lift state up_. If one needs data in
two different components, one should simply lift
it up to the nearest parent component.

What would be this component state, is passed by
as a prop. What would be its actions, are now
given in the form of callbacks to update state
in the parent component.

For example, when the value of the editor changes, the
contract component fires the `update_text` callback.


The state and callbacks are described in a
Contract module, detailed in @lst:contractmodule


```{#lst:contractmodule .reason caption="The contract module"}
module Contract = {
  type t = {
    editor: ref(option(Monaco.monaco)),
    decorate: ref((. string, (int, int, int, int)) => unit),
    text: string,
    address: string,
    balance: string,
    storage: string,
    typechecks: bool,
    errors: option(Error.typecheck_error),
    typemap: Typemap.t,
    shown_typetrans: option(Typemap.trans),
    timers: list(Js.Global.timeoutId),
  };

  type callbacks = {
    update_text: string => unit,
    update_storage: string => unit,
    update_balance: string => unit,
    load_editor: Monaco.monaco => unit,
    load_decorate: ((. string, (int, int, int, int)) => unit) => unit,
    delete_me: unit => unit,
    update_typemap: (. int, int) => unit,
  };
```

The timer callbacks are used to fire a
typechecking request after a text change if none
have been made in the last 300 milliseconds.

`update_storage` fires the typecheck timer as
well. `delete_me` is a callback for - as its
name implies - deletion, as there can be several
contract components on the page, each with its
`Delete me` button. The delete button is inside
the contract component, but needs to interact
with its parent component. Therefore, the parent
gives the corresponding callback to its child.


As for the other props, `is_tracing` is used to
gray out several buttons as those are not
available when running operations. `is_alone` is
used so that the last contract can not be
deleted. `id` is the id of the contract.
`server_connected` is to gray out the indent
button if the server cannot be reached. `items`
is used for the preset systems which is
elaborated on in its own section.

Visually, the component is split in two. On the
left side, one can find the editor, inputs for
storage and balance, as well as buttons to
upload or download a contract. On the other
side, one can find type information and whether
the parsing and typechecking failed.