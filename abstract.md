# Abstract

Blockchain are becoming more and more relevant.
Notably through their use as decentralized
platforms. Tezos is a blockchain launched in
June 2018 and supports programs hosted on the
blockchain, smart contracts. Smart contracts are
key to the platform. They need to be robust and
auditable. They need to be simple to write and
audit. This can be achieved with the aid of
specialized tools.

During this internship, I developed a tool to aid in
the writing of smart contract as well as a an
distributed application running on the Tezos
blockchain.
