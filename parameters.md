## Parameters form

In order to call a contract, an user must fire a
transaction. The transaction parameters, as well
as the bootstrap account's balance are stored in
the `ParamsForm` component. This component
leverages the `re-formality` library. It works
like a functor. For each field, one must write a
module containing both an update function and a
validator. For example, a bootstrap account
field validator will check if the value entered
is actually a natural number.

Then, one must use this module as parameter to
the corresponding input. For example, the input
for the gas_limit will be as described in figure

```reason
<input
  className="input"
  placeholder="Gas limit"
  value={form.state.gas_limit}
  disabled={form.submitting}
  onBlur={_ => form.blur(GasLimit)}
  onChange={event =>
    form.change(
      GasLimit,
      ParamsForm.GasLimitField.update(
        form.state,
        event->ReactEvent.Form.target##value,
      ),
    )
  }
/>
```

There are five parameters to the initial transaction.
* The parameter with which to call the contract with
* The gas limit, which puts a bound on parsing, typechecking
  and execution
* The amount of mutez the user wishes to transfer
* The id of the contract to direct this transaction to
* The identity of the payer - one of the bootstrap accounts.

This form is wrapped inside a drop card component.

