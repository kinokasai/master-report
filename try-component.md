
## Try Component

This is the biggest component of the project. It has the
responsibility of managing the contracts, of calling the
server with the correct information, and of general management.

### Contracts

Contracts are stored in the form of an association list.
While an hashmap could prove more efficient for a big number
of contracts, try-michelson currently only allows for five
different contracts.

Each contract is identified by an id, ranging from 0 to 5.
These contracts can be modified by the actions
detailed in @lst:trycomponent.

```{#lst:trycomponent .reason caption="The controller component"}
type action =
  | UpdateTypemap(int, Typemap.t)
  | UpdateErrors(int, option(Error.typecheck_error))
  | UpdateText(int, string)
  | UpdateStorage(int, string)
  | UpdateBalance(int, string)
  | UpdateTypechecks(int, bool)
  | ShowTypestack(int, int, int)
  | CreateContract(Presets.contract_content)
  | DeleteContract(int)
 
type state = {
  active_ids: list(int),
  contracts: list((int, Contract.t)),
  id_exists: ref(int => bool),
  mode,
  server_connected: bool,
  params: Presets.params,
  delta_key: string /* This is used to know when to reload form */
};
```

`Update*` actions are what make the most of the callback
given to the contract component. For example, the body of
the callback `update_text` is 
`(text) -> self.send(UpdateText(id, text))`.
Contract components are created from the active_ids list,
through a map.

`ShowTypestack` behaves like an `Update` for the displayed
type information.

`CreateContract` adds a new id to the `active_ids` list, as
well as a new contract to the map, associated the the new
id.

`DeleteContract`, as it name implies, deletes the
corresponding contract. However, the `active_ids` list must
be contiguous, since the ids are linked to monaco's internal
model. So we need to shift each id coming after the deleted
contract.

`LoadScenario` replaces all contracts stored with the
contracts contained in its payload.

### Server connection

`server_connected` informs on whether the server could be
reached. It's used to display an error message if it could
not. Its corresponding actions are `Ping(int)` and
`UpdateConnection(bool)`.

`Ping` takes a duration as a payload. If the server could
not be reached, the client pings it at a decreasing rate -
up to one minute.

`UpdateConnection` is simply used to update the value of
`server_connected`.

### Parameters

`params` stores the current value of transaction parameters.
This will be passed down as a prop to the form component.
Its associated actions are `UpdateParams(Presets.params)` and
`LoadScenario(Presets.scenario)`.

`UpdateParams` updates the `params` value with the
associated params payload. However, the form is encapsulated
as described in the Parameter form section, and does not
refresh on a prop change. To force the refresh, we also need
to pass it down a different key, in the form of `delta_key`.

`LoadScenario` updates the `params` value with the
associated value contained in the scenario payload.

`id_exists` is passed down to the `ParamsForm`
component as a prop, as the form needs to
know if a certain id exists for the id field's
validator. Since the id list is not constant, we
need to forward a ref, which will be updated
every time an id is created or deleted.

### Editing mode

`mode` is a variant type that holds two values:
`Traveling(travel_state)` and `Edition(edition_state)`.

Those two modes are exclusive, as it would not
make sense for an user to edit the contract as
he's tracing its execution. 

`edition_state` is a record solely containing an
`error` field. Its associated actions are
`GlobalError(string)`, which simply sets the
`error` to its payload. It's used notably by the
server connection system.

### Travel mode

`travel_state` is a fair bit more rich, it's what contains
the data corresponding to the operation chain.

```{#lst:travelstate .reason caption="The travel state"}
type travel_state = {
  snapshot_index: int,
  history: list(Snapshot.t),
  current_snapshot: Snapshot.t,
  trace_state: option(trace_state),
  show_trace: bool,
};
```

A snapshot is comprised of an operation and its result -
called a _receipt_ - as well as the pending operations.

The associated actions are
`LoadHistory(History.t)`,
`LoadReceipt(Receipt.t)`, `NextOp`, `PreviousOp`
and `ToggleTrace`. Of course, these actions only
have effect when the mode is set to `Traveling`.

`LoadHistory` sets the travel state based on its payload.

`LoadReceipt` loads the current receipt. If the current
operation is a transaction to a contract, it retrieves the
execution trace and builds a zipper. It is called by
`LoadHistory`, `NextOp`, and `PreviousOp`.

`NextOp` and `PreviousOp` allow for movement in the
operation chain.

`ToggleTrace` toggles the show_trace boolean, which controls
whether the user can trace the contract's execution step by
step. It's invoked through a button on the operation.