## Dropdown

The dropdown is an interactive dropdown menu. It is used to
select an item into a list, and doing the corresponding
side-effect.

![A dropdown menu](img/dropdown.png){#fig:dropdown width=20%}

```reason
type state = {
    is_active: bool,
    active_item: string,
    contents: list(Elt.t),
};

module Elt = {
  type t = {
    id: string,
    effect: unit => unit,
  };
};

type action =
    | Select(string)
    | Activate
    | Deactivate
    ;

let make = (~contents, ~active_item) => {
 
```

`is_active` informs or whether to display the full menu.
`active_item` logs which one is selected to both display it
in the title, and highlight it in the menu.
Lastly, `contents` holds the different choices.
The `Elt.t` are represented as links which `onClick`
property fires their effect function.

```reason
<a
    className="dropdown-item"
    onClick={_ => {
        dispatch(Select(elt.id));
        elt.effect();
    }}>
    elt.id->ReasonReact.string
</a>;
```

The `Select` action updates the `active_item` while the
`Activate` and `Deactivate` sets the `is_active` property
accordingly. We can't use a single `Switch` action as
there's cases where we just want to deactivate the menu,
e.g. a click outside the menu.