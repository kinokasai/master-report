# State of the art

Michelson is a relatively low-level language.
For easier development, Tezos' ecosystem offers
higher-level languages built on top of
Michelson, such as _ligo_ [@ligo] and
_liquidity_ [@liquidity]. 

### Try Liquidity

Try liquidity is an online editor in which to
edit Liquidity code. On top of
editing, there's support for compiling and
decompiling Liquidity code. Try Liquidity also
allows for step-by-step stack inspection based
on liquidity instructions.

Contrarily to Try Michelson, there is no support
for step-by-step execution of Michelson code.
Try Liquidity also does not support inspection
of the internal operations emitted by a
contract, nor multi-contract simulation.
There's also no documentation and no completion.

### Emacs mode

Emacs possesses a plugin to aid in writing
Michelson code. It has syntax highlighting, and
typing information. It connects to a full-blown
Tezos node in order to retrieve typing
information.

Try Michelson connects to its own server, built
on top of the protocol for this purpose, it is
thus more lightweight.

Try Michelson offers several features on top of
this: completion, documentation, inspection of a
call chain and step-by-step execution.

### Remix

In the Ethereum ecosystem, Remix is a web
development environment for Solidity, Ethereum's
smart contract language. It has support for
edition, execution, debugging, testing and
analysis of solidity contracts.

Remix is a much more mature project than Try
Michelson. While Try Michelson offers some
of the features that Remix does, it lacks some
others. Remix is an inspiration for Try Michelson.