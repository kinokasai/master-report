## Dropcard

```ocaml
type state = {
    header: reactElement;
    content: reactElement;
    dropped: bool;
}

type action =
    | Switch

let make = (~header, ~content) => ...
```


This is a simple _dropcard_ component. It is comprised of
two `reactElement` and a `boolean`. If the boolean is set to
true, then it should display both elements. Otherwise, just
display the header element. The `Switch` action carries no
payload, as the `header` and `content` elements are given as
props. However, it has for effect to flip the `dropped` boolean.

![A dropcard](img/dropcard.png)