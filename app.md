# Applications

Tezos' success is dependent on its adoption. The
project can attract new users through several
means. Better tooling, such as Try Michelson,
makes getting into Tezos easier. Demo
applications are a good way to ease developers
into developing their own applications for the
project. The proposed contribution consists in a
basis for developing such applications, as well
as a voting application.

## Base

Demo applications are developed using
ReasonReact native (RRN). The rationale for using
Reason and ReasonReact are highlighted in
[@sec:techoiceclient]. RRN allows, on top of a
web target, to build applications targeting
mobile platforms such as Android and iOS.

The framework provides a build system in the
form of `yarn` and `BuckleScript`. It also
supplies a tiny client library.

## Voting application

This demo application is a voting app. It
presents the user with two choices,
"Chocolatine" and "Pain au chocolat". The
interface is coded with RRN. The app speaks to a
blockchain using the provided client library.

The application does not originate a contract on
launch. The contract has been originated
separately and is independent of the application.

### Smart contract

An user of the contract is able to vote provided
they pay a small price - say 5 tez.

[Listing @lst:votecode] represents the code of the
voting contract as well as its initial storage.

```{#lst:votecode .michelson caption="A voting contract and its initial storage"}
storage (map string int);
parameter string;
code {
  AMOUNT;
  PUSH mutez 5000000;
  ASSERT_CMPGT;
  DUP;
  DIP {
    CDR;
    DUP
  };
  CAR;
  DUP;
  DIP { 
    GET; 
    ASSERT_SOME;
    PUSH int 1;
    ADD;
    SOME
  };
  UPDATE;
  NIL operation;
  PAIR
}

{Elt "Chocolatine" 0; Elt "Pain au chocolat" 0}
```

The description of the storage and parameter
types is given on lines 1-2. Then the code of
the contract is given.
On line 4, `AMOUNT` pushes on the stack the
amount - in mutez - sent to the contract address
by the user. The threshold amount (5tez) is also
pushed on the stack on line 6 and compared to
the amount sent: `ASSERT_CMPGT` asserts that the
threshold is indeed greater than the required
amount. If that happen to not be the case, the
contract execution stops. If it did, the
execution continues on line 7, where the stack
is prepared for the next action: `DUP` copies
the top of the stack, then we manipulate the
tail of the stack while preserving its head
using `DIP`: there, we take the right element of
the pair (the chosen proposition), and we
duplicate it again. By closing the block guarded
by `DIP`, we recover the former stack's top, and
the following line take its left element with
`CAR`, and duplicates it.

On line 14, we use `DIP` to protect the top of
the stack again. `GET` then pops `chosen` and
`proposals` from the stack, and pushes an
option containing the number of votes of the
proposal, if it was found in the map. If it
was not found, `ASSERT_SOME` makes the program
fail. On line 15, the number of votes is
incremented by `ADD`, and packed into an option
type by `SOME`.

We then leave the `DIP` block to regain access
to the value at the top of the stack (`chosen`).
On the next line, `UPDATE` pops the three values
remaining on top of the stack, and pushes the
`proposals` map updated with the incremented
value for `chosen`. Finally, we push an empty
list of operations with `NIL operation`, and
pair the two elements on top of the stack to get
the correct return type.
