---
institute: Sorbonne University
title: Internship Report
subtitle: Nomadic Labs
author:
- Alexandre Doussot
teacher: Pietro Abate
course: M2 STL
place: Paris
date: \today
papersize: a4
  - "figure"
  - "figures"
header-includes:
  - \usepackage{tikz}
  - \usepackage{listings}
  - \usepackage{color}
  - \usepackage{courier}
  - \usepackage{syntax}
  - \usepackage{fancyvrb}
  - \usepackage{booktabs}
---

\include tex_templates/code.tex

\newpage

\include abstract.md

\newpage

\include tezos.md

\newpage


\include try-michelson.md

\newpage

\pagebreak

\clearpage

\newpage

\include client.md


\newpage

\pagebreak

\clearpage

\newpage

\include editor.md

\newpage


\include stack.md

\include preset.md


\newpage


\include contract.md

\newpage

\include try-component.md


\newpage

\include step_by_step.md

\newpage

\pagebreak

\clearpage

\newpage

\include app.md

\newpage

\include conclusion.md

\include lit_review.md

\newpage

\include appendix.md

\newpage

# Bibliography