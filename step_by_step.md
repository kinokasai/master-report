# Step-by-step execution

Michelson makes a trade off between
robustness and ease of programming. Programming
in a stack-based language without an idea of the
stack makes things difficult. Michelson
contracts are also made to be audited, a task
for which step-by-step execution helps.

One of the main features of Try Michelson is its
support for interactive step-by-step execution
of smart contracts. The user must be able to
_step_ - that is to say continue to the next
instruction without going into a scope if the
current instruction starts one (i.e. a loop, an
if...) - to _step in_ - which implies going
inside the scope - or _go up_ - go to the
encompassing instruction.

The server provides an execution trace in the
form of a list of stack states. A stack state is
comprised of a _location_ (where the instruction
that produced this stack is located), a _stack_,
and the _gas_ left when this state is attained.
@fig:tracelist represents the execution trace
corresponding to the contract in
@lst:linearcontract.

The trace information retrieved from the server
must be transformed into a tree structure. Try
Michelson uses a combination of trees and
_Zippers_ [@zip] to represent execution traces.
Zippers allow for fast bi-directional movement
around a tree. They can be viewed as the
functional equivalent of a pointer.

![An execution list](graph/trace_list.dot.png){#fig:tracelist width=50%}

The first step is to filter and sort the list.
We neither want the stacks for locations
that are out of the code nor do we want to take
macro expanded instructions into account.
Michelson programs reading mostly linearly means
that every instruction coming later in the code
must come later in execution order.
@fig:linear tree represents the tree-shaped
trace of the code represented in @lst:linearcontract

```{#lst:linearcontract .michelson caption="A linear contract"}
code {
  PUSH int 3;
  DROP;
}
```

For example, in this code, `PUSH` is followed naturally by `DROP`.

![A tree for linear code](graph/linear_tree.dot.png){#fig:lineartree width=20%}


The order is chosen based on the position of the
instructions. If an instruction is positioned
inside of another, then a child is created.
[@fig:nesttree] represents the tree associated with @lst:nestcode


```{#lst:nestcode .michelson caption="Nested Michelson code"}
code {
  PUSH int 3;
  DIP { PUSH bool True};
  DROP;
  DROP;
}
```

\newpage

In this code, `DROP` must be the instruction
following `DIP` on step. It is added as its
brother. `PUSH int 4` is located inside the
`DIP`, it is then inserted as a child.


![A tree for nested code](graph/tree.dot.png){#fig:nesttree width=30%}

The algorithm is still incomplete. The tree
would be ill-shaped if the code would contain
loops. The sorting order described above would
place them out of order - putting each
instructions n times instead of the sequence n
times. @fig:incorrectlooptree represents an
incorrect tree associated to @lst:loopcode.

To remedy that, we re-visit the tree and
sort each siblings by gas left.
@fig:correctlooptree represents the correct loop
tree associated to @lst:loopcode.

```{#lst:loopcode .michelson caption="Looping Michelson code"}
parameter unit;
storage unit;
code { PUSH int 2 ;
       PUSH bool True ;
       LOOP { PUSH int 1 ; SWAP ; SUB ; DUP ; GT } ;
       DROP ;
}
```

![Incorrect loop tree](graph/loop_tree.dot.png){#fig:incorrectlooptree width=70%}

![Correct loop tree](graph/correct_loop_tree.dot.png){#fig:correctlooptree width=70%}