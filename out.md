---
institute: Sorbonne University
title: Internship Report
subtitle: Nomadic Labs
author:
- Alexandre Doussot
teacher: Pietro Abate
course: M2 STL
place: Paris
date: \today
papersize: a4
  - "figure"
  - "figures"
header-includes:
  - \usepackage{tikz}
  - \usepackage{listings}
  - \usepackage{color}
  - \usepackage{courier}
  - \usepackage{syntax}
  - \usepackage{fancyvrb}
  - \usepackage{booktabs}
---

\setmonofont{FantasqueSansMono Nerd Font Mono}

\makeatletter

\patchcmd{\lsthk@SelectCharTable}{%
  \lst@ifbreaklines\lst@Def{`)}{\lst@breakProcessOther)}\fi
}{%
}{
}{
}

\makeatother

\newcommand{\CodeSymbol}[1]{\textcolor{cyan!50!black}{#1}}
\newcommand{\CodeSymbolPurple}[1]{\textcolor{purple}{#1}}

\lstset{
basicstyle=\footnotesize\ttfamily\bfseries\color{red},
keywordstyle=\color{green!50!black},
keywordstyle=[2]\color{red!50!black},
keywordstyle=[3]\color{purple},
keywordstyle=[4]\color{red},
stringstyle=\color{yellow!80!black},
commentstyle=\itshape\color{gray!50!white},
identifierstyle=\color{cyan!50!black},
literate={\{}{{\CodeSymbol{\{}}}1
               {\}}{{\CodeSymbol{\}}}}1
               {(}{{\CodeSymbol{(}}}1
               {)}{{\CodeSymbol{)}}}1
               {[}{{\CodeSymbol{[}}}1
               {]}{{\CodeSymbol{]}}}1
               {=}{{\CodeSymbol{=}}}1
               {;}{{\CodeSymbol{;}}}1
               {.}{{\CodeSymbol{.}}}1
               {:}{{\CodeSymbol{:}}}1
               {|}{{\CodeSymbol{|}}}1
               {,}{{\CodeSymbol{,}}}1
               {-}{{\CodeSymbol{-}}}1
               {1}{{\CodeSymbol{1}}}1
               {2}{{\CodeSymbol{2}}}1
               {3}{{\CodeSymbol{3}}}1
               {4}{{\CodeSymbol{4}}}1
               {5}{{\CodeSymbol{5}}}1
               {6}{{\CodeSymbol{6}}}1
               {7}{{\CodeSymbol{7}}}1
               {8}{{\CodeSymbol{8}}}1
               {9}{{\CodeSymbol{9}}}1
               {0}{{\CodeSymbol{0}}}1
               {->}{{\CodeSymbol{->}}}1
               {<-}{{\CodeSymbol{<-}}}1
               {+}{{\CodeSymbol{+}}}1
               {-}{{\CodeSymbol{-}}}1
               {*}{{\CodeSymbol{*}}}1
}

\lstdefinelanguage{SOL}{
keywords={},
morestring=[b]{"},
morestring=[b]{'},
morecomment=[l]{//},
morecomment=[s]{/*}{*/},
morecomment=[s][\color{black}]{<}{>}
}

\lstdefinelanguage{JavaScript}{
morekeywords={break,case,catch,continue,debugger,default,delete,do,else,finally,
  for,function,if,in,theninstanceof,new,return,switch,this,throw,try,typeof,var,void,while, True, False,
  node, with, machine, memory,instances, +, prototype, add,mul,div,(not-and), list, let, print, scan type, of, loadjmp, jmp},
morekeywords=[2]{interface, class,enum,export,extends,import,super, implements,package,private,protected,public,static,yield},
morekeywords=[3]{Lt, Gt, Eq, Const, Temp, Binop, Load, Alloc, Nand, CTemp, LoadJmp, RelOffset, Scan, True, False, Int, Binop, Not, Or, And, Move, Store, Label, Jump, String, Var, Print, Let, If, Scan},
morekeywords=[4]{}
morestring=[b]{"},
morestring=[b]{'},
morecomment=[l]{//},
morecomment=[s]{/*}{*/},
morecomment=[s][\color{black}]{<}{>}
}

\lstdefinelanguage{michelson}{
 % list of keywords
 morekeywords={
     ABS,
     ADD,
     ADDRESS,
     AMOUNT,
     AND,
     BALANCE,
     BLAKE2B,
     CAR,
     CAST,
     CDR,
     CHECK\_SIGNATURE,
     COMPARE,
     CONCAT,
     CONS,
     CONTRACT,
     CREATE\_ACCOUNT,
     CREATE\_CONTRACT,
     DIP,
     DROP,
     DUP,
     EDIV,
     EMPTY\_MAP,
     EMPTY\_SET,
     EQ,
     EXEC,
     FAILWITH,
     GE,
     GET,
     GT,
     HASH\_KEY,
     IF,
     IF\_CONS,
     IF\_LEFT,
     IF\_NONE,
     IMPLICIT\_ACCOUNT,
     INT,
     ISNAT,
     ITER,
     LAMBDA,
     LE,
     LEFT,
     LOOP,
     LOOP\_LEFT,
     LSL,
     LSR,
     LT,
     MAP,
     MEM,
     MUL,
     NEG,
     NEQ,
     NIL,
     NONE,
     NOT,
     NOW,
     OR,
     PACK,
     PAIR,
     PUSH,
     RENAME,
     RIGHT,
     SELF,
     SENDER,
     SET\_DELEGATE,
     SHA256,
     SHA512,
     SIZE,
     SLICE,
     SOME,
     SOURCE,
     STEPS\_TO\_QUOTA,
     SUB,
     SWAP,
     TRANSFER\_TOKENS,
     UNIT,
     UNPACK,
     UPDATE,
     XOR,
 },
 sensitive=true, % keywords are not case-sensitive
 morecomment=[l]{\#}, % l is for line comment
 morecomment=[s]{(}{)}, % s is for start and end delimiter
 morestring=[b]", % defines that strings are enclosed in double quotes
 morekeywords=[2]{
     bool,
     contract,
     int,
     key,
     key\_hash,
     lambda,
     list,
     map,
     big\_map,
     nat,
     option,
     or,
     pair,
     set,
     signature,
     string,
     bytes,
     mutez,
     timestamp,
     unit,
     operation,
     address,
 },
}


\lstset{basicstyle=\footnotesize\ttfamily,breaklines=true}
\lstset{escapeinside={(*@}{@*)}}

\newpage

# Abstract

Blockchain are becoming more and more relevant.
Notably through their use as decentralized
platforms. Tezos is a blockchain launched in
June 2018 and supports programs hosted on the
blockchain, smart contracts. Smart contracts are
key to the platform. They need to be robust and
auditable. They need to be simple to write and
audit. This can be achieved with the aid of
specialized tools.

During this internship, I developed a tool to aid in
the writing of smart contract as well as a an
distributed application running on the Tezos
blockchain.

\newpage

# Introduction

## Blockchain

Blockchains can be seen as an immutable database
operating in a decentralized network. They are
built upon several key concepts and tools:

* They heavily use cryptography to ensure users
  authentication as well as the database
  immutability
* They offer a probabilistic solution to the
  "Byzantine generals problem" [@byzantine] for
  consensus among all participants - which we
  will call nodes in the rest of this document -
  in the decentralized network
* They use a peer to peer gossip network for
  low-level communications between the nodes.

Blockchains are often called _crypto-ledgers_ as
they can be seen as an electronic book,
recording transactions where users' identity and
book immutability are cryptographically ensured.

In order to validate and append transactions to
the ledger, all blockchains follow a similar
generic algorithm. A transaction is a token
transfer from an account to an account (it can
be itself).

1. New transactions are broadcast to all nodes
   which aggregate them in blocks.
2. The next block is broadcast by one or several 
   nodes.
3. Nodes express their acceptance of a block by
   including its cryptographic hash in the next
   block they create.

In abstract terms, a Blockchain is built upon
two functions, `apply` and `score`. `apply`
takes the current state and a block to produce a
new state. `score` computes the score of a state
to choose the preferred one between multiple
states.

Three main properties of the blockchain are
_liveness_, _finality_ and _safety_.

Liveness ensures that progress is possible. In
terms of consensus, it is the guarantee that all
actors will eventually agree on a value.

Safety is the guarantee that nothing bad will
ever happen. In a consensus mechanism, this
means that no two actors ever decide on
different values.

Finality is the guarantee that a block will not
be revoked and that past transaction can never
change.

Blockchains face some common distributed systems
challenges. To be resilient to Sybil attacks
[@sybil] a solution is to restrict the pool of
block producers by tying it to the use of a
scarce resource. A difficulty is to choose this
resource and create incentives that push the
majority of the network to be honest.
Restricting the pool of block producers can also
lead to liveness issues that have to be taken
into account to make sure that the chain does
not stop whenever a block producer is offline.
Blockchains also have to consider malicious
block producers and performance issues such as
network delays. Each blockchain provides its own
set of solutions to overcome these challenges
but most of them rely on the foundations set by
Bitcoin [@nakamoto2008bitcoin]. Blockchains also
face the risk of forks: to update their economic
protocol, blockchains have to go through social
consensus and risk frictions in their user
community that may lead to the birth of hard
forks splitting the community in two parts
agreeing on two different chains. Such hard fork
occurred after the Ethereum DAO hack [@daohack]
leading to the birth of two blockchains:
Ethereum Classic and Ethereum.

### Bitcoin

Bitcoin was introduced in 2008. The main
objective behind Bitcoin was to propose a
decentralized electronic cash system. Bitcoin is
the name of the blockchain as well as the name
of its cryptocurrency. Blockchains use tokens to
represent assets stored in the chain. Some of
these tokens, as in Bitcoin, can represent a
currency. Bitcoin is also a decentralized system
(the actual blockchain) allowing users to store
and transfer their tokens. This system is the
combination of peer-to-peer (P2P) network
associated with a consensus protocol to maintain
consistency between nodes. This consensus
algorithm introduced the notion of
_Proof-of-work_ (PoW). With PoW, the scarce
resource used to restrict the pool of block
producers - which Bitcoin calls "miners" - is
computing power. High computing power mostly
demands very efficient computing hardware
associated with high energy consumption. The
main idea is to request that miners compete in
the solving of a puzzle to earn the right to
produce (mine) the next block. With PoW, Sybil
attacks are difficult and expensive. Associated
with incentives to motivate miners to compete,
liveness is easy to achieve. In case of _soft
forks_ (temporary split) of the chain, which can
be caused by bugs, network latency or malicious
mining, and to maintain consistency of the chain
among all the nodes, each node has to decide
which branch is likely to be the winning chain.
This is done by computing the _score_. The node
starts from a tree of branches, and at each
iteration, it looks at the score of all branches
and picks the winner. Bitcoin’s consensus
algorithm specifies to always keep the longest
chain between two forks. A chain is considered
longer if its total difficulty (summing the
difficulty of the puzzles solved to mine each
block of the chain) is higher.

The difficulty is adapted every two weeks to
maintain an average of 10-minute intervals
between two consecutive blocks depending on the
global computing power of the miners. PoW is the
solution currently used by most blockchains.


### Decentralized platforms

While Bitcoin focused on electronic cash, it
also comes with the concept of decentralized
computations: Bitcoin's script allows many
interesting forms of _in-transaction_
computation, and others quickly proposed to use
blockchains to build decentralized computing
platforms [@buterin2014ethereum]. This was popularized by the current
second-biggest blockchain, Ethereum, in 2014,
which pushed the concept further. The main idea
is to see blockchains as vending machines where
users can pay for a service. In blockchain
platforms, these services are small programs
living on the chain. Users of the blockchain can
store code in blocks and other users can execute
this code. These programs are called _Smart
Contracts_. They can, of course, perform
blockchain operations, such as token transfers,
but they can also be used for access control or
to interact with others. Some smart contracts,
being fed with off-chain information, serve as
_Oracles_ selling trusted information. Smart
contracts can be used in many applications such
as financial contracts, developing new
currencies on top of the basic crypto-currency
of the blockchain, such as voting systems,
games, or crowdfunding.

## Tezos specifics

The Tezos blockchain was presented in 2014 and
released in 2018. Contrarily to most new
blockchains at the time of its presentation, it
not based on PoW.

### Proof-of-stake

The current Tezos protocol is based on a _Liquid
Proof-of-Stake_ (LPoS) consensus algorithm.

PoS is very different from PoW. It considers the
stake - the number of tokens - that users hold
as the primary resource used to build the pool
of block producers - called _bakers_ in the
Tezos ecosystem. In the current Tezos consensus
protocol, to push a block at a certain level,
bakers are randomly selected using a lazy
infinite priority list of baking slots. In order
to participate in this random selection, a baker
must hold at least a roll of tokens -
corresponding to 8000 tokens at the time of this
writing. The number of baking slots is
proportional to the number of rolls that a baker
holds. However, participants that do not hold
enough tokens or who do not wish to bake blocks
can delegate their tokens to another baker, much
like in Liquid Democracy one can delegate their
right to vote. They keep the ownership of their
tokens but increase the stake of their delegate
in the random assignment of baking slots.
Delegation makes the PoS system fairer and
participative and helps balance a possible
concentration of tokens in a few hands.

Finality is the guarantee that a block will not
be revoked and that past transaction can never
change.
In order to help the chain reach finality faster,
 Tezos' PoS mechanism introduces
endorsements of
baked blocks. For each baked block, 32
endorsements - signatures - slots are created,
allowing chosen bakers to approve a block by
signing it. Using endorsements, the highest
block resulting score is considered the head of
the chain where the score is:

`score(block_n + 1) = 1 + score(block_n) + nb_endorsements`

In order to provide incentives to bakers for
participating in the network, the protocol
rewards baking and endorsing.

Tezos' LPoS consensus algorithm, _via_ its
internal mechanism and its associated
incentives, solves the challenges of the
platform without requiring significant
computational power. It also focuses on the
users of the platform instead of external actors
as it is possible with PoW: only stake-holders
can participate but all can delegate their
stake.

### Self-amending blockchain

Tezos is a self-amending blockchain. The
protocol that validates blocks and implements
the consensus algorithm can amend itself.
Concretely, a new protocol is downloaded from
the network, compiled, and hot-swapped to
replace the current one.

The amendment procedure can be triggered in
several ways, depending on the protocol. In the
current Tezos economic protocol, an amendment
can only be triggered as the result of an
on-chain voting procedure.

It helps avoiding forks of the chain and reduces
friction in the community. A protocol amendment
may consist of very important upgrades such as a
switch to a completely different consensus
protocol. It can also consist in smaller
modifications such as extending the smart
contract language, modifying the rewarding
system to enforce network participation or
adding new kinds of transactions, for instance
anonymous ones.

## Tezos as a decentralized platform

Tezos' economic protocol not only handles a
registry of transactions, but also has support
for smart contracts.

Smart contracts are small programs registered in
the blockchain together with a private data
storage, but the data is publicly visible. A
contract registered in the chain is said to be
_originated_ and has an address prefixed by
`KT1`, which is given in the contract
origination's block.

They are executed by performing specific
transactions to their associated account (their
address). The transaction carries data that is
passed as a program parameter and can thus be
viewed as a procedure call.

The execution of a smart contract can change the
state of its storage and trigger on-chain
operations. A contract can call another contract
by this mechanism. Such operations are called
_internal operations_.

Smart contract languages are usually
Turing-complete. However, the replicated nature
of the contract storage and the liveliness
requirement of the consensus algorithm imposes
some restrictions on their execution.

### Limited execution time

Any call to a smart contract, once included in a
block, will be executed on every node in the P2P
network, as they have to validate the block
before including it in their view of the chain
and before passing it to their neighbors. This
means that the execution time of each smart
contract call included in a block has to fit
multiple times in the inter-block time of the
chain to ensure its liveness.

In order to restrict the computation cycles
allocated to each smart contract execution, each
instruction is associated to a _gas_ unit,
crafted to be proportional to the actual
execution time, and the smart contract to the
maximum number of gas units allowed to burn. The
interpreter running the smart contract burns an
amount of gas which is crafted to be
proportional to the actual execution time. If an
execution exceeds its allowed gas consumption,
it is stopped immediately and the effects of the
execution are rolled back. The transaction is
still included in the block and the fees are
taken, to prevent the nodes from being spammed
with failing transactions.

In Tezos, the economic protocol sets the gas
limit per block and for each transaction, and
the emitter of the transaction also sets an
upper bound for its transaction's gas
consumption. Each baker can set their own
strategy for accepting or refusing a block. The
default strategy requires the transaction fee to
be proportional to the gas upper bound.

## Michelson

Michelson is the language in which the smart
contract scripts are written. The Michelson
language has been designed before the launch of
the Tezos blockchain. The most important parts
of Michelson's implementation, the typechecker
and the interpreter, belong to the economic
ruleset of Tezos - the protocol - so that the
language can evolve through the Tezos amendment
voting process.

### Design Rationale

Smart contracts operate in a very constrained
context: they need to be expressive, evaluated
efficiently, and their resource consumption must
be accurately measured as their execution time
impacts block construction and propagation.
Smart contracts are non-updatable programs that
can handle valuable assets, there is thus a need
for strong guaranties on the correctness of
these programs.

The need for efficiency and more importantly for
accurate account of resource consumption leans
toward a low-level interpreted language, while
the need for contract correctness leans toward a
high level, easily auditable, easily
formalisable language, with strong static
guaranties.

To satisfy these constraints, Michelson was made
a Turing-complete, low level, stack based
interpreted language - not unlike _Forth_ -
enabling resource measurement, but with some
high-level ML-like features: polymorphic
products, options, sums, lists, sets, and maps
data-structures with collection iterators,
cryptographic primitives and anonymous
functions. Instructions are pure functions that
take a stack as input and return a stack as
output.

### Shape {#sec:michshape}

A Michelson contract script is written in three
parts: the parameter type, the storage type, and
the code of the contract. The code consists in
one block of code that can only be called with
one parameter, but multiple entry points can be
encoded by branching on a nesting of sum types.

When the contract is originated on the chain, it
is bundled with a data storage which can then
only be changed by the successful execution of a
contract. The parameter and the contract's
associated storage are paired and passed to the
contract's code at each execution, at the end of
which it has to return a list of operations and
the updated storage.

As usual in stack-based languages, Michelson
code consist of a sequence of instructions. Each
instruction takes its parameters on the stack,
and put their result back on the stack. Each
instruction encodes a stack transformation.
Instructions can be divided in three types:
_simple_, _nesting_, and _macros_. Simple
instructions are basic, like `DUP`, which
duplicates the top value of the stack. Nesting
instructions can be parameterized by other
instructions. That's the case of `DIP`. In `DIP
{ DUP }`, the body of the `DIP` instruction is
executed on the second value of the stack.
Macros are not primitive instructions. They are
expanded to a sequence of primitive
instructions. `DUUP` is a macro that expands to
`DIP { DUP }; SWAP`.



### A short example

In @lst:meaningless, we present a
simple example, annotated by indices
denoting each stack change. The following graph
shows the state of the stack at each annotated
point.

```{#lst:meaningless .michelson caption="A simple example"}
parameter unit;
storage unit;
code { (0)
  PUSH int 2 ; (1)
  PUSH bool True ; (2)
  LOOP { (3)
    PUSH int 1 ; (4)
    SWAP ; (5)
    SUB ; (6)
    DUP ; (7)
    GT (8)
  } ;
  DROP ; (9)
}
```

0. The contract loads its storage and parameter
1. `2` is pushed onto the stack
2. `True` is pushed onto the stack
3. The code starts to loop. `LOOP` takes a
   boolean as parameter.
4. `1` is pushed onto the stack
5. The first and second elements of the stack
   are swapped
6. The second element is subbed from the first
7. The top element is duplicated
8. `GT` pushes `True` if the top element is
   greater than 0, `False` else
9. `DROP` pops the first value of the stack.


![Stack evolution](graph/meaningless_stack_evo.dot.png){#fig:stackevo}

It can be difficult for an human programmer to
keep the stack in mind, particularly so with
complex contracts. A stack-based language is
easier to reason about, but is also is a
compromise as it then becomes more difficult for
programmers.

## Interacting with the Tezos blockchain

The binary architecture is centered around two
main components.

Firstly, the _node_ is responsible for
connection to peers through the gossip network
and updating the ledger's state - also called
_context_. As all the blocks and operations are
exchanged between nodes on the network, the node
is able to filter and propagate data from/to its
connected peers. Using the blocks received on
the gossip network, the nodes keeps an
up-to-date context.

Secondly, the _client_ is the main tool to
interact with a Tezos blockchain node.

There are currently three public Tezos networks:

* `mainnet`, the current incarnation of the
  Tezos blockchain. It runs with real tez (Tezos
  tokens) that have been baked or allocated to
  the donors of 2017's fundraiser. It has been
  live since June 30th 2018.
* `alphanet`, which is based in the `mainnet`
  codebase but uses free tokens. It is the
  reference network for developers wanting to
  test their software.
* `zeronet` which is the testing network, with
  free tokens and frequent resets.

It is also possible to run a "localhost-only"
network, through sandboxed node and client.

### Sandbox node

When a new network is bootstrapped, the network
is initialized with a dummy economic protocol,
called _genesis_. It is possible to run the same
protocol as alphanet, by launching the
`tezos-activate-alpha` command, which will
inject the current protocol.



### Tezos' architecture (in the large)

![Tezos' octopus](img/octopus.svg){#fig:octopus}

The characteristic that makes Tezos unique is
its self-amending property. The part that amends
itself is called the _economic protocol_,
sometimes abbreviated by protocol. 


The protocol only sees one blockchain - a linear
sequence of blocks since the genesis (the first
block). It does not know that lives in an open
network where nodes can propose alternative heads.

The rest of a Tezos node is called the _shell_.
Only the shell knows about the multiple heads.
It is responsible for choosing between the
various chain proposals coming from the bakers.
The shell has the responsibility of selecting
and downloading alternative chains, feed them to
the protocol, which in turn has the
responsibility to check them for errors, and
give them an absolute score. The shell then
simply selects the valid head of highest
absolute score. This part of the shell is called
the validator.

The rest of the shell includes the peer-to-peer
layer, the disk storage of blocks, the
operations to allow the node to transmit the
chain data to new nodes and the versioned state
of the ledger. In-between the validator, the
peer-to-peer layer and the storage sits a
component called the distributed database, that
abstracts the fetching and replication of new
chain data to the validator.

Protocols are compiled using a tweaked OCaml
compiler (green part on the left of the picture)
that does two things. First, it checks that the
protocol’s main module has the right type. A
good analogy is to see protocol as plug-ins, and
in this case, it means that it respects the
common plugin interface. Then, it restricts the
typing environment of the protocol’s code so
that it only calls authorized modules and
functions. Seeing protocols as plug-ins, it
means that the code only called primitives from
the plug-in API. It is a form of statically
enforced sandboxing.

Finally, the RPC layer (in yellow on the right
in the picture) is an important part of the
node. It is how the client, third party
applications and daemons can interact with the
node and introspect its state. This component
uses the mainstream JSON format and HTTP
protocol. It uses in-house libraries
ocplib-resto and ocplib-json-typed (via the
module Data_encoding). It is fully
inter-operable, and auto descriptive, using JSON
schema.

## The protocol

The in-work protocol in called protocol Alpha.
Protocol Alpha, whose Alpha has nothing to do
with the one in Alphanet, is the name of the
initial economic protocol. Alpha is a
placeholder name, while is decided the naming
convention for protocol versions.

As all protocols, Alpha is made of a series of
Ocaml files.

### Alpha context

The Proof-of-Stake algorithm, as described in
the whitepaper [@whitepaper], relies on an
abstract state of the ledger, that is read and
transformed during validation of a block.

Due to the polymorphic nature of Tezos (the
protocol is subject to amendments), the ledger's
state - called *context* in the code - cannot be
specific to protocol Alpha's needs. The PoS is
thus implemented over a generic key-value store
whose keys and associated binary data must
implement the abstract structure.

### Protocol modules

The protocol notably implements the
`Script_interpreter` and `Script_ir_translator`,
which are required to parse and run Michelson
programs.

### Client modules

Helper modules such as `Michelson_v1_parser` and
`Michelson_v1_error_reporter` are implemented
inside `lib_client`, that is to say outside of
the protocol.

## Contribution

As of February 2019, the only way to get
assistance in the writing of a Michelson smart
contract is to use an emacs plugin named
michelson-mode. It connects to a full-blown
Tezos node in order to retrieve typing
information.

This simplistic approach was both too
restrictive and too heavy. Tezos is a relatively
young project, whose future success is linked to
adoption. People who (will) write smart
contracts aren't necessarily developers at
heart. Complicated tools such as emacs and
relative unfamiliarity with Tezos' software
ecosystem - launching your first node is
non-trivial - could hinder such adoption.

The proposed contribution has three goals: Not
embedding more than necessary, not forcing any
particular editor on the user, and reusing as
much code as possible from the node.

## Outline

This project is divided in two parts.

1. A server, built on top of the protocol, which
   exposes remote procedure calls for Michelson
   development helpers, such as typechecking and
   execution of Michelson smart contracts.
2. A web client, exposing an editor with modern
   features, such as syntax highlighting,
   linting, on-the-fly typechecking, completion,
   and step-by-step debugging.


\newpage



# Server

The server has been realized in OCaml. It
exposes RPC endpoints using Tezos' own lib,
`tezos-rpc-http`. It depends on the client
libraries and is currently implemented on top of
the version 004 of the protocol. The project is
deployed using docker. The source code is hosted
on gitlab.

The two main existing RPCs in the server are
`typecheck` and `apply`.

## The context problem{#sec:ctxt}

Michelson is a language in which to express
smart contracts that are supposed to be run *on
chain*. Running Michelson code without a chain
environment doesn't make sense. For instance,
the `ADDRESS` instruction - which is supposed to
put on the stack the address of the called smart
contract - does not have any sense without a
chain context.

In the sandbox node, we can see that it creates
and initializes a dummy context, in which
pre-created contracts, called "bootstrap
contracts", are initialized with an arbitrary
address and balance. The context is essential.
It holds information about everything relevant
to the chain, such as the balance of every
account and any contract ever originated.

A local chain can be created by following the
following steps.

* Create a genesis block of correct hash
  (`BLockGenesisGenesisGenesisGenesisGenesisf79b5d1CoW2`).
  A genesis block is the first block of any
  blockchain. It sets the parameters and
  constants of the blockchain. Constants include
  the time between each block, how many tokens
  make a roll, and much more.
  //FIXME ask why it must have that hash
* Add the bootstrap accounts to the header's
  context
* Initialize the protocol using the genesis block

This chain of actions provides a context object.
The context holding the ledger's state, it is
necessary to

## A note about parsing{#sec:parsing}

Tezos' internal functions use a token indexing
for referencing source code. For instance, in
the snippet `PUSH int 3`; the `PUSH int 3`
instruction will be token #1, `int` #2, curly
and `3` the third token. While efficient, this
sometimes proves impractical. The server is
meant to be used by clients which need to display
information to their users about the code in
terms of line and columns, not tokens.

Michelson's parser main function returns a
`parsing_result`. It is comprised of various
elements.

* The source code which produced this result
* An unexpanded, but linted form of the parsed code
* An expansion table, which links token indices
  to a full location (line, column and character
  offset for the start and end of the token).
* An unexpansion table, which links instructions
  created by macro expansion to their original
  instruction. For instance, take the following
  Michelson code, in which `DUUP` will be
  expanded as `DIP { DUP }; SWAP`.

```michelson
code {
    PUSH int 0;
    PUSH bool True;
    DUUP;
}
```
![Michelson code featuring the DUUP macro](img/blank.png){#fig:duupmacro}

@fig:exptable details the expansion and
unexpansion table stemming from the parsing of
the code present in @fig:duupmacro.

![Expansion table & unexpansion table](graph/expansion_table.dot.png){#fig:exptable width=80%}

### The token problem

The typemap object has the following shape:

```ocaml
type typemap = {
    location: int;
    before: type_stack;
    after: type_stack
} list
```

A typemap is a list detailing the typing of
every stack attained through the program. The
client needs to be able to show the user how the
stack is typed at specific points of their
choosing. This form is too restrictive for that,
as the location is expressed in term of tokens.
Fortunately, the ast object gained from the
parser contains an expansion table from which we
can extract a code location.
 accomplish anything chain-related.

## Architecture

The server is divided in three main modules,
`Server`, `Driver` and `Michelson_v1_json`. For
practical reasons, the `Models` module also
exists in order to share data structures between
the main modules.

### Server

`Server`, which defines the following services (RPC
endpoints):

* `hello`, which is for liveness checks.
* `expand`, to expand a Michelson expression
* `indent`, for linting purposes
* `typecheck`, which typechecks a contract
* `get_trace`, which traces the execution of a
  operation chain.

The `Server` module is simply an interface for
requests. It calls `Driver` for any
computational task.

### Driver

This is the main module. It creates a
context according the parameters given by
`Server`. It calls `Michelson_v1_json`
before returning.

### Michelson_v1_json {#sec:michjson}

`Michelson_v1_json` transforms the OCaml data
structures into usable json data using
data-encoding.

### Typechecking sequence

Figure @fig:typeseq details the call
sequence when the server receives a typecheck
request.

![Typecheck request sequence](out/update_text/typecheck_sequence.png){#fig:typeseq}

When the server receives a typecheck request
from a client, it starts by decrypting the
request (Converting from Json to OCaml
structures). This is done using the Data
Encoding library. A typecheck request is made
of two fields: the code of the contract, and its
storage. It then calls `Driver`'s `typecheck`
function with the decrypted code and storage.
In turn, the `Driver` parses the code to get a
parsing result, described in [@sec:parsing]. Once this is done, the module creates
a new context from scratch, using the steps
described in [@sec:ctxt]. It then calls the
protocol's parser to check if the storage's
value is compatible with the storage type
enunciated in the code. In turn, it calls the
protocol's typechecker in order to get a type map.
Finally, it calls `Michelson_v1_json` to
transform the typemap in Json, before returning
it back to the server.



## Run

One of the main features of Try Michelson is to allow users to
run their contract step-by-step, and inspect the *values* on
the stack at each point.

The trace function is a primitive of the tezos
library, with the following signature.

`context -> ~source: contract -> ~payer: contract -> self(contract, script) -> ~parameter -> ~amount: tez -> Error_monad(trace)`


This function takes several parameters.

* A context, obtained through the initialization
  of a local chain
* The source contract - the identifier of the caller
* The payer - the contract that pays the fees.
  (Those two can be given a bootstrap account
  address).
* Information about the called
  contract itself. Its address - which we need
  for instructions such as `SELF` and `ADDRESS`
  - and its script - its code. 
* A call parameter - which type
  must match the one given in the script - as
  well as the amount transferred to the called
  contract. This information has to be given by
  the user. 

The functions returns a wrapped trace object of
the following shape:

```ocaml
type trace = {
    location: int;
    gas_left: int;
    stack: {item: string} list;
}
```

As explained in [@sec:michseq], there's two
types of Michelson instructions: _primitive_
instructions and _macro_ instructions. One of
the features of the proposed client, needs to
highlight whichever instruction is being ran to
produce a given stack during step-by-step
execution. However, in the case of macros, we
don't want to show the inner instructions (`DIP
{ DUP}; SWAP`), just the unexpanded macro
(`DUUP` in our example). The parsing result
object features an unexpansion table, which
links a token id to its origin id. Our algorithm
proceeds in the following way.


1. Find where a given token stems from using
   the unexpansion table. It will be itself if
   not part of a macro.
2. Get its location through the expansion table.
   1. If none is found, give it a special value -
      we chose zero.
   1. If some is found, check its dependencies.
      Dependencies are obtained through the
      expansion table. 
3. If the token matches the last element of that
   list, it means it was present in the
   unexpanded code. It points to either a simple
   instruction or the whole macro, and is marked
   as such.

```ocaml
let location_from_token(token) ->
  let unexpanded_token = unexpansion_table[token]
  let location, deps = expansion_table[unexpanded_token]
  if location == null then
    return 0 (* The token could not be found *)
  else
    let last = deps[len(deps - 1)]
    if last === token
        return location, false (* The token was in the unexpanded code *)
    else
        return location, true (* The token comes from an expansion *)
```

While this method works for simple contracts, it
is deeply unsatisfactory. It is not
possible to specify the balance of the called
contract. More importantly, the typechecking
fails if the contract refers to another contract
in its source code. These shortcomings prevents
us from being able to simulate a multi-contract
call chain - which is a significant feature.

### A more complete approach

Instead of merely throwing away the context
after each call, we must forward it to the next
operation if one exists. We also have to
consider all the operations that a contract can
emit - `Reveal`, `Transaction`, `Origination`,
and `Delegation` - and the balance of all
participants in the operation chain.

The user must now provide every known contract -
made of address, code, storage and balance - the
balance of each bootstrap account, as well as
the transaction parameters - parameter, gas,
amount, called address, payer.


One of this project's initial goals is not to
re-code what's already in the codebase. Tezos'
codebase features the apply module, in which
those concerns have been taken care of.
We want the user to be able to run through their
code step-by-step, and the apply module doesn't
allow that. We can find that in the
middle of the file, the code calls
`Script_interpreter.execute`, which executes the
code without providing an execution trace. We
could amend that, but that's code present in the
protocol, which means that we would need to wait
for the next amendment to propose the change,
and it would need to be voted in. Thus, we
decided on the simplest solution. We duplicated
the `apply.ml` file, applied the changes -
namely adding tracing (which implies to rewrite
all the data structures present in the original
GADT) - and added it in a single commit. While
this works, the developer now has to manually
merge the apply file. However, for the `mainnet`
branch, this only has to be done roughly every 6
months.

### Contract origination

Another challenge is to have originated all the
contracts before executing the chosen one. While
we could generate origination operations for
each contract to apply to the initial context,
we would have to notify the user at which
address each contract was originated, which
would prove frictionful. Tezos' codebase
features a mechanism similar to the bootstrap
accounts, bootstrap contracts, which is used
by shaping the contract objects in the right
shape. This approach has the interesting
particularity that each contract will always be
originated at the same address based on the
order of origination. This allows us to bypass
the back and forth that would have happened
using the operation method.

Figure @fig:applyact details how the apply
algorithm proceeds.

![Apply algorithm operation](out/update_text/apply_loop.png){#fig:applyact width=33%}





\newpage

\pagebreak

\clearpage

\newpage

# Client

## Technological choices

Try-michelson is mostly written in ReasonML.
Small parts have been coded in Typescript.
Several factors have influenced this decision.
While having no experience in Reason, Nomadic
Labs is primarily an OCaml shop. Reason is
proving to be a more pleasant and robust way to
build web applications using react, leveraging
OCaml's type system - it can interesting to note
that react was initially prototyped in SML.

As for the text editor, our choice fell on the
increasingly popular editor, monaco. Its
development speed as well as exposing a lot of
features while keeping usage relatively simple
led it to be the de facto editor in web space.

Keeping the UI in sync with the state is
non-trivial. That's why a number of JavaScript
library have started popping in the last few
years - such as Angular, Vue, or React.

React is a declarative and efficient JavaScript
library for building user interfaces. It lets
you compose complex UIs from small and isolated
pieces of code called “components”. Recently, a
Facebook team started work on a wrapper for
reactjs called _ReasonReact_. It builds upon the
foundation laid by reactjs, but is a ReasonML
library. Reason is a perfect fit for React, as
its core principles - immutability,
functional programming and type system - are
built into Reason itself.

## Components

### A react primer

In ReasonReact, we can differentiate a number of components.
The most basic ones - called stateless - are purely
graphical. However, each component can be instantiated by a
number of elements, called _props_. For example, a message
component may take a prop of type `string`, the message
text.

A more interesting and usual component is the *reducer*. In
addition to its graphical part, it possesses both _state_, and
a way to act on it, _actions_. It applies the actions
through a `reducer` or `dispatch` method, which is often
implemented as a pattern matching on the state|action tuple.

In this report, we'll describe the react components
implemented during the course of this project by describing
their state and actions, then showing the result in picture
form.

### About bindings

As ReasonML is a statically typed language,
typing operability with JavaScript comes with a
cost. If wanting to use a JavaScript library
while developing in Reason, one must beforehand
write type bindings for each function one wants
to call. While a bit cumbersome, a Reason
application can have access to whole JavaScript
ecosystem.

## Architecture

![Component architecture](out/update_text/architecture.png)

![Try Michelson UI](img/ui-try-michelson.png){width=90%}

The Controller component is the main component of the
application. It embeds four other sub
components.

* `History` shows the operation chain stemming
  from the first transaction. 
* `ParamsForm` is the form in which the user
  specifies the context in which the contracts
  will be run. 
* `StackComponent` prints the
  stacks when the user is tracing a contract's
  execution.
* The `Contract` component is host to the editor
  itself, and contains everything contract
  related. There can be several `Contract`
  components displayed at the same time. Let us
  now see how they interact through some
  scenarios.

### Submit

![Submit sequence diagram](out/update_text/submit_sequence.png){#fig:submitseq width=75%}

In @fig:submitseq, we show what happens when an
user submits a transaction.

When an user clicks on the submit button in the
`ParamsForm`, the form calls it submit callback given by the
`Controller`. The `Controller` then emits a trace request to
the server, which responds with an history. In turn, the
`Controller` loads that history, builds any zipper tree that
may exist, and changes its internal state, which is
reflected to the user in the form of an history inspecting
interface.


\newpage

\pagebreak

\clearpage

\newpage

## The editor

We've decided to use the package `react-monaco-editor` as to
not wrap it ourselves. However, `react-monaco-editor` is a
JavaScript package, so we have to write typing bindings.

Fortunately, Reason and ReasonReact introduce a lot of
helpers to alleviate the pain of writing those. For example,
we can get access to the a JavaScript class through `@bs.module`.

### A Typescript interlude

Monaco is written in Typescript, which means the
easier and safer way to use it is through
Typescript. Based on this observation and advice
by the creator of `sketch` @sketch, we decided
to write most of the editor logic in Typescript,
and bind those functions to Reason later on.
This has the benefit of massively reducing the
surface for which to write bindings. Monaco is
also undergoing rapid development making its API
change often, which makes it a bit of a moving
target. Fortunately, as both Typescript and
Reason transpile to JavaScript, interoperability
is possible between the two.

### Stack printing

In order to achieve feature parity with emacs'
michelson-mode, we need to be able to show the
user typing information corresponding to its
cursor position. While its fairly easy to get
access to the cursor position in Typescript
land, it's not in Reason land. Thus, we created
a Typescript function `onCursorMove`, that takes
a callback. When the editor is initialized in
Reason, we call that function with a `dispatch`
method, which is a reducer function changing the
type stack value in the Reason state.

### Hover

One of Try-michelson advantages over its emacs
companion is the hover feature. When an user
hovers over an instruction, a tooltip containing
documentation is displayed. This uses monaco's
`hoverProvider` API. The documentation has be
leveraged from the `rst` file used in the online
documentation [@tezosdoc].

### Completion

Try-michelson also allows for completion. This uses monaco's
`completionItemProvider` API. It finds what could be
meaningful suggestions in the context, and lets monaco do
the filtering. Fortunately, Michelson is both a concise and
elegantly devised language, so there is not much place for
confusion. Everything that follows a `{` or `;` and starts
with an uppercase letter must be a primitive. Similarly,
everything that follows a primitive and starts with a
lowercase letter must be a type.

### Syntax Highlighting

Finally, syntax highlighting is provided through monaco's
`monarchTokensProvider`. Its usage is quite simple as it's
like building a small lexer. The user just has to provide
regexps and ids corresponding to that regex.

[Listing @lst:regex] details the supplied regular expressions.

```{#lst:regex .typescript caption="Token regexes"}
[/[0-9]+/, "number"]
[/[A-Z][A-Z]+/, "primitive"]
[/[A-Z][a-z]+/, "constant"]
[/\"[^\"]*\"/, "string"]
[/#.*$/, "comment"]
[/(code|parameter|storage)/, "section"]
[/[a-z]+/, "type"]
```



\newpage


## Stack Component

The stack component is an example of a stateless
component. Its only function is to display a
graphically correct depiction of a stack, which
is given as a prop.

In Try-michelson, each stack is displayed next
to another stack, either the stack before the
instruction is executed, or after (depending on
which one it is). It means that each stack must
be bottom-aligned, hence the `max_stack_size`
prop.

Then, values in Michelson can be arbitrarily
large - Pairs, long addresses, hashes... We
decided to limit the max width of a stack.
Through experimentation, we settled on 32
characters max per row, as well as centered
text.

[Listing @lst:padfun] details the pad function
used to center the text.

```{#lst:padfun .reason caption="The pad function"}
let pad = (max_width, s) => {
  let len = String.length(s);
  if (len > max_width) {
    String.sub(s, 0, max_width - 3) ++ "...";
  } else {
    let space = (max_width - len) / 2;
    String.make(space, ' ') ++ s ++ String.make(space + 1, ' ');
  };
};
```


## Presets

Try-michelson has a pedagogical aspect to it. In that light,
we've decided to develop a preset system. Presets are stored
in json form, under the aptly-named file `presets.json`. It
allows other teams, such as the Michelson team to add or
alter those presets without touching the code.

There's two types of presets. Contract presets, and
scenarios.

### Contract presets

Contract presets, like their name suggest, describe
contracts. Basically, it's comprised of three of the four
contract components - the code, the storage and the balance - plus a name. This serves several purposes. Firstly, the
presets are presented through the dropcard component
presented above. Secondly, for a given scenario, one might
want to reference an already-described contract.

### Scenario

Scenarios encompass more than their contract counterpart.
Indeed, not only they can be comprised of several contracts,
they also have to express the balance of each bootstrap
account, as well as the parameter of the leading transaction.


\newpage


## Contract Component

The Contact component is an example of a
_phantom_ component. In its bubble, this should
be a reducerComponent. After all, its state
changes - when the user types, for example.
However, it does not hold any state. The reason
for this is that what would be its own state is
needed outside of this component.

The website needs access to the code for
typechecking, to the contract as a whole, to
send for tracing. There's no way in react to
query a component for its state. Instead, the
mantra is _lift state up_. If one needs data in
two different components, one should simply lift
it up to the nearest parent component.

What would be this component state, is passed by
as a prop. What would be its actions, are now
given in the form of callbacks to update state
in the parent component.

For example, when the value of the editor changes, the
contract component fires the `update_text` callback.


The state and callbacks are described in a
Contract module, detailed in @lst:contractmodule


```{#lst:contractmodule .reason caption="The contract module"}
module Contract = {
  type t = {
    editor: ref(option(Monaco.monaco)),
    decorate: ref((. string, (int, int, int, int)) => unit),
    text: string,
    address: string,
    balance: string,
    storage: string,
    typechecks: bool,
    errors: option(Error.typecheck_error),
    typemap: Typemap.t,
    shown_typetrans: option(Typemap.trans),
    timers: list(Js.Global.timeoutId),
  };

  type callbacks = {
    update_text: string => unit,
    update_storage: string => unit,
    update_balance: string => unit,
    load_editor: Monaco.monaco => unit,
    load_decorate: ((. string, (int, int, int, int)) => unit) => unit,
    delete_me: unit => unit,
    update_typemap: (. int, int) => unit,
  };
```

The timer callbacks are used to fire a
typechecking request after a text change if none
have been made in the last 300 milliseconds.

`update_storage` fires the typecheck timer as
well. `delete_me` is a callback for - as its
name implies - deletion, as there can be several
contract components on the page, each with its
`Delete me` button. The delete button is inside
the contract component, but needs to interact
with its parent component. Therefore, the parent
gives the corresponding callback to its child.


As for the other props, `is_tracing` is used to
gray out several buttons as those are not
available when running operations. `is_alone` is
used so that the last contract can not be
deleted. `id` is the id of the contract.
`server_connected` is to gray out the indent
button if the server cannot be reached. `items`
is used for the preset systems which is
elaborated on in its own section.

Visually, the component is split in two. On the
left side, one can find the editor, inputs for
storage and balance, as well as buttons to
upload or download a contract. On the other
side, one can find type information and whether
the parsing and typechecking failed.

\newpage


## Try Component

This is the biggest component of the project. It has the
responsibility of managing the contracts, of calling the
server with the correct information, and of general management.

### Contracts

Contracts are stored in the form of an association list.
While an hashmap could prove more efficient for a big number
of contracts, try-michelson currently only allows for five
different contracts.

Each contract is identified by an id, ranging from 0 to 5.
These contracts can be modified by the actions
detailed in @lst:trycomponent.

```{#lst:trycomponent .reason caption="The controller component"}
type action =
  | UpdateTypemap(int, Typemap.t)
  | UpdateErrors(int, option(Error.typecheck_error))
  | UpdateText(int, string)
  | UpdateStorage(int, string)
  | UpdateBalance(int, string)
  | UpdateTypechecks(int, bool)
  | ShowTypestack(int, int, int)
  | CreateContract(Presets.contract_content)
  | DeleteContract(int)
 
type state = {
  active_ids: list(int),
  contracts: list((int, Contract.t)),
  id_exists: ref(int => bool),
  mode,
  server_connected: bool,
  params: Presets.params,
  delta_key: string /* This is used to know when to reload form */
};
```

`Update*` actions are what make the most of the callback
given to the contract component. For example, the body of
the callback `update_text` is 
`(text) -> self.send(UpdateText(id, text))`.
Contract components are created from the active_ids list,
through a map.

`ShowTypestack` behaves like an `Update` for the displayed
type information.

`CreateContract` adds a new id to the `active_ids` list, as
well as a new contract to the map, associated the the new
id.

`DeleteContract`, as it name implies, deletes the
corresponding contract. However, the `active_ids` list must
be contiguous, since the ids are linked to monaco's internal
model. So we need to shift each id coming after the deleted
contract.

`LoadScenario` replaces all contracts stored with the
contracts contained in its payload.

### Server connection

`server_connected` informs on whether the server could be
reached. It's used to display an error message if it could
not. Its corresponding actions are `Ping(int)` and
`UpdateConnection(bool)`.

`Ping` takes a duration as a payload. If the server could
not be reached, the client pings it at a decreasing rate -
up to one minute.

`UpdateConnection` is simply used to update the value of
`server_connected`.

### Parameters

`params` stores the current value of transaction parameters.
This will be passed down as a prop to the form component.
Its associated actions are `UpdateParams(Presets.params)` and
`LoadScenario(Presets.scenario)`.

`UpdateParams` updates the `params` value with the
associated params payload. However, the form is encapsulated
as described in the Parameter form section, and does not
refresh on a prop change. To force the refresh, we also need
to pass it down a different key, in the form of `delta_key`.

`LoadScenario` updates the `params` value with the
associated value contained in the scenario payload.

`id_exists` is passed down to the `ParamsForm`
component as a prop, as the form needs to
know if a certain id exists for the id field's
validator. Since the id list is not constant, we
need to forward a ref, which will be updated
every time an id is created or deleted.

### Editing mode

`mode` is a variant type that holds two values:
`Traveling(travel_state)` and `Edition(edition_state)`.

Those two modes are exclusive, as it would not
make sense for an user to edit the contract as
he's tracing its execution. 

`edition_state` is a record solely containing an
`error` field. Its associated actions are
`GlobalError(string)`, which simply sets the
`error` to its payload. It's used notably by the
server connection system.

### Travel mode

`travel_state` is a fair bit more rich, it's what contains
the data corresponding to the operation chain.

```{#lst:travelstate .reason caption="The travel state"}
type travel_state = {
  snapshot_index: int,
  history: list(Snapshot.t),
  current_snapshot: Snapshot.t,
  trace_state: option(trace_state),
  show_trace: bool,
};
```

A snapshot is comprised of an operation and its result -
called a _receipt_ - as well as the pending operations.

The associated actions are
`LoadHistory(History.t)`,
`LoadReceipt(Receipt.t)`, `NextOp`, `PreviousOp`
and `ToggleTrace`. Of course, these actions only
have effect when the mode is set to `Traveling`.

`LoadHistory` sets the travel state based on its payload.

`LoadReceipt` loads the current receipt. If the current
operation is a transaction to a contract, it retrieves the
execution trace and builds a zipper. It is called by
`LoadHistory`, `NextOp`, and `PreviousOp`.

`NextOp` and `PreviousOp` allow for movement in the
operation chain.

`ToggleTrace` toggles the show_trace boolean, which controls
whether the user can trace the contract's execution step by
step. It's invoked through a button on the operation.


\newpage

# Step-by-step execution

Michelson makes a trade off between
robustness and ease of programming. Programming
in a stack-based language without an idea of the
stack makes things difficult. Michelson
contracts are also made to be audited, a task
for which step-by-step execution helps.

One of the main features of Try Michelson is its
support for interactive step-by-step execution
of smart contracts. The user must be able to
_step_ - that is to say continue to the next
instruction without going into a scope if the
current instruction starts one (i.e. a loop, an
if...) - to _step in_ - which implies going
inside the scope - or _go up_ - go to the
encompassing instruction.

The server provides an execution trace in the
form of a list of stack states. A stack state is
comprised of a _location_ (where the instruction
that produced this stack is located), a _stack_,
and the _gas_ left when this state is attained.
@fig:tracelist represents the execution trace
corresponding to the contract in
@lst:linearcontract.

The trace information retrieved from the server
must be transformed into a tree structure. Try
Michelson uses a combination of trees and
_Zippers_ [@zip] to represent execution traces.
Zippers allow for fast bi-directional movement
around a tree. They can be viewed as the
functional equivalent of a pointer.

![An execution list](graph/trace_list.dot.png){#fig:tracelist width=50%}

The first step is to filter and sort the list.
We neither want the stacks for locations
that are out of the code nor do we want to take
macro expanded instructions into account.
Michelson programs reading mostly linearly means
that every instruction coming later in the code
must come later in execution order.
@fig:linear tree represents the tree-shaped
trace of the code represented in @lst:linearcontract

```{#lst:linearcontract .michelson caption="A linear contract"}
code {
  PUSH int 3;
  DROP;
}
```

For example, in this code, `PUSH` is followed naturally by `DROP`.

![A tree for linear code](graph/linear_tree.dot.png){#fig:lineartree width=20%}


The order is chosen based on the position of the
instructions. If an instruction is positioned
inside of another, then a child is created.
[@fig:nesttree] represents the tree associated with @lst:nestcode


```{#lst:nestcode .michelson caption="Nested Michelson code"}
code {
  PUSH int 3;
  DIP { PUSH bool True};
  DROP;
  DROP;
}
```

\newpage

In this code, `DROP` must be the instruction
following `DIP` on step. It is added as its
brother. `PUSH int 4` is located inside the
`DIP`, it is then inserted as a child.


![A tree for nested code](graph/tree.dot.png){#fig:nesttree width=30%}

The algorithm is still incomplete. The tree
would be ill-shaped if the code would contain
loops. The sorting order described above would
place them out of order - putting each
instructions n times instead of the sequence n
times. @fig:incorrectlooptree represents an
incorrect tree associated to @lst:loopcode.

To remedy that, we re-visit the tree and
sort each siblings by gas left.
@fig:correctlooptree represents the correct loop
tree associated to @lst:loopcode.

```{#lst:loopcode .michelson caption="Looping Michelson code"}
parameter unit;
storage unit;
code { PUSH int 2 ;
       PUSH bool True ;
       LOOP { PUSH int 1 ; SWAP ; SUB ; DUP ; GT } ;
       DROP ;
}
```

![Incorrect loop tree](graph/loop_tree.dot.png){#fig:incorrectlooptree width=70%}

![Correct loop tree](graph/correct_loop_tree.dot.png){#fig:correctlooptree width=70%}

<!-- \newpage

\pagebreak

\clearpage

\newpage -->

# Applications

Tezos' success is dependent on its adoption. The
project can attract new users through several
means. Better tooling, such as Try Michelson,
makes getting into Tezos easier. Demo
applications are a good way to ease developers
into developing their own applications for the
project. The proposed contribution consists in a
basis for developing such applications, as well
as a voting application.

## Base

Demo applications are developed using
ReasonReact native (RRN). The rationale for using
Reason and ReasonReact are highlighted in
[@sec:techoiceclient]. RRN allows, on top of a
web target, to build applications targeting
mobile platforms such as Android and iOS.

The framework provides a build system in the
form of `yarn` and `BuckleScript`. It also
supplies a tiny client library.

## Voting application

This demo application is a voting app. It
presents the user with two choices,
"Chocolatine" and "Pain au chocolat". The
interface is coded with RRN. The app speaks to a
blockchain using the provided client library.

The application does not originate a contract on
launch. The contract has been originated
separately and is independent of the application.

### Smart contract

An user of the contract is able to vote provided
they pay a small price - say 5 tez.

[Listing @lst:votecode] represents the code of the
voting contract as well as its initial storage.

```{#lst:votecode .michelson caption="A voting contract and its initial storage"}
storage (map string int);
parameter string;
code {
  AMOUNT;
  PUSH mutez 5000000;
  ASSERT_CMPGT;
  DUP;
  DIP {
    CDR;
    DUP
  };
  CAR;
  DUP;
  DIP { 
    GET; 
    ASSERT_SOME;
    PUSH int 1;
    ADD;
    SOME
  };
  UPDATE;
  NIL operation;
  PAIR
}

{Elt "Chocolatine" 0; Elt "Pain au chocolat" 0}
```

The description of the storage and parameter
types is given on lines 1-2. Then the code of
the contract is given.
On line 4, `AMOUNT` pushes on the stack the
amount - in mutez - sent to the contract address
by the user. The threshold amount (5tez) is also
pushed on the stack on line 6 and compared to
the amount sent: `ASSERT_CMPGT` asserts that the
threshold is indeed greater than the required
amount. If that happen to not be the case, the
contract execution stops. If it did, the
execution continues on line 7, where the stack
is prepared for the next action: `DUP` copies
the top of the stack, then we manipulate the
tail of the stack while preserving its head
using `DIP`: there, we take the right element of
the pair (the chosen proposition), and we
duplicate it again. By closing the block guarded
by `DIP`, we recover the former stack's top, and
the following line take its left element with
`CAR`, and duplicates it.

On line 14, we use `DIP` to protect the top of
the stack again. `GET` then pops `chosen` and
`proposals` from the stack, and pushes an
option containing the number of votes of the
proposal, if it was found in the map. If it
was not found, `ASSERT_SOME` makes the program
fail. On line 15, the number of votes is
incremented by `ADD`, and packed into an option
type by `SOME`.

We then leave the `DIP` block to regain access
to the value at the top of the stack (`chosen`).
On the next line, `UPDATE` pops the three values
remaining on top of the stack, and pushes the
`proposals` map updated with the incremented
value for `chosen`. Finally, we push an empty
list of operations with `NIL operation`, and
pair the two elements on top of the stack to get
the correct return type.

\newpage

# Conclusion & Future work

The demo application has never been released.
Try Michelson has successfully been used both as
a working and training tool. Nomadic Labs
provides training sessions regularly. Try
Michelson has been used during these workshops
and has proven especially popular. It is
available online at [](https://try-michelson.tzalpha.net)

### Server

The server could incorporate the "smart parts"
currently present in Try Michelson, and expose
an API corresponding to Language Server Protocol
[@lsp]. This would allow the server to be used
in conjunction with many modern editors
supporting lsp. Experimentation has been started
in this direction.

### Client

Besides adapting to the server's lsp API, Try
Michelson could go undergo an interface change.
The space given to the editor is in fact rather
small proportionally to its usage.

Client code must be made more modular to provide
standalone components that can be reused in
other applications.

Future work for Try Michelson also includes the
integration of _Mi-cho-coq_, a coq
framework for proving smart contracts.

The goal is to provide the final user a complete
environment to write, simulate, prove, and
deploy smart contracts on the Tezos blockchain.

### Apps

The framework could be enriched with other
applications, demoing other kinds of smart
contract.

## Code and artifacts

All the code is available under the MIT license.

* Try Michelson: `gitlab.com/nomadic-labs/try-michelson.git`
* Server: `gitlab.com/nomadic-labs/tezos-lang-server.git`
* Application: `gitlab.com/kinokasai/tezos-dapps.git`
* Visual Studio Code extension: `gitlab.com/kinokasai/vscode-michelson.git`

# State of the art

Michelson is a relatively low-level language.
For easier development, Tezos' ecosystem offers
higher-level languages built on top of
Michelson, such as _ligo_ [@ligo] and
_liquidity_ [@liquidity]. 

### Try Liquidity

Try liquidity is an online editor in which to
edit Liquidity code. On top of
editing, there's support for compiling and
decompiling Liquidity code. Try Liquidity also
allows for step-by-step stack inspection based
on liquidity instructions.

Contrarily to Try Michelson, there is no support
for step-by-step execution of Michelson code.
Try Liquidity also does not support inspection
of the internal operations emitted by a
contract, nor multi-contract simulation.
There's also no documentation and no completion.

### Emacs mode

Emacs possesses a plugin to aid in writing
Michelson code. It has syntax highlighting, and
typing information. It connects to a full-blown
Tezos node in order to retrieve typing
information.

Try Michelson connects to its own server, built
on top of the protocol for this purpose, it is
thus more lightweight.

Try Michelson offers several features on top of
this: completion, documentation, inspection of a
call chain and step-by-step execution.

### Remix

In the Ethereum ecosystem, Remix is a web
development environment for Solidity, Ethereum's
smart contract language. It has support for
edition, execution, debugging, testing and
analysis of solidity contracts.

Remix is a much more mature project than Try
Michelson. While Try Michelson offers some
of the features that Remix does, it lacks some
others. Remix is an inspiration for Try Michelson.

\newpage

# Appendix

In this appendix are detailed two react
components.

## Dropcard

```ocaml
type state = {
    header: reactElement;
    content: reactElement;
    dropped: bool;
}

type action =
    | Switch

let make = (~header, ~content) => ...
```


This is a simple _dropcard_ component. It is comprised of
two `reactElement` and a `boolean`. If the boolean is set to
true, then it should display both elements. Otherwise, just
display the header element. The `Switch` action carries no
payload, as the `header` and `content` elements are given as
props. However, it has for effect to flip the `dropped` boolean.

![A dropcard](img/dropcard.png)

\newpage

## Dropdown

The dropdown is an interactive dropdown menu. It is used to
select an item into a list, and doing the corresponding
side-effect.

![A dropdown menu](img/dropdown.png){#fig:dropdown width=20%}

```reason
type state = {
    is_active: bool,
    active_item: string,
    contents: list(Elt.t),
};

module Elt = {
  type t = {
    id: string,
    effect: unit => unit,
  };
};

type action =
    | Select(string)
    | Activate
    | Deactivate
    ;

let make = (~contents, ~active_item) => {
 
```

`is_active` informs or whether to display the full menu.
`active_item` logs which one is selected to both display it
in the title, and highlight it in the menu.
Lastly, `contents` holds the different choices.
The `Elt.t` are represented as links which `onClick`
property fires their effect function.

```reason
<a
    className="dropdown-item"
    onClick={_ => {
        dispatch(Select(elt.id));
        elt.effect();
    }}>
    elt.id->ReasonReact.string
</a>;
```

The `Select` action updates the `active_item` while the
`Activate` and `Deactivate` sets the `is_active` property
accordingly. We can't use a single `Switch` action as
there's cases where we just want to deactivate the menu,
e.g. a click outside the menu.

\newpage

# Bibliography
