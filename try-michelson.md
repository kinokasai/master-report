
# Server

The server has been realized in OCaml. It
exposes RPC endpoints using Tezos' own lib,
`tezos-rpc-http`. It depends on the client
libraries and is currently implemented on top of
the version 004 of the protocol. The project is
deployed using docker. The source code is hosted
on gitlab.

The two main existing RPCs in the server are
`typecheck` and `apply`.

## The context problem{#sec:ctxt}

Michelson is a language in which to express
smart contracts that are supposed to be run *on
chain*. Running Michelson code without a chain
environment doesn't make sense. For instance,
the `ADDRESS` instruction - which is supposed to
put on the stack the address of the called smart
contract - does not have any sense without a
chain context.

In the sandbox node, we can see that it creates
and initializes a dummy context, in which
pre-created contracts, called "bootstrap
contracts", are initialized with an arbitrary
address and balance. The context is essential.
It holds information about everything relevant
to the chain, such as the balance of every
account and any contract ever originated.

A local chain can be created by following the
following steps.

* Create a genesis block of correct hash
  (`BLockGenesisGenesisGenesisGenesisGenesisf79b5d1CoW2`).
  A genesis block is the first block of any
  blockchain. It sets the parameters and
  constants of the blockchain. Constants include
  the time between each block, how many tokens
  make a roll, and much more.
  //FIXME ask why it must have that hash
* Add the bootstrap accounts to the header's
  context
* Initialize the protocol using the genesis block

This chain of actions provides a context object.
The context holding the ledger's state, it is
necessary to

## A note about parsing{#sec:parsing}

Tezos' internal functions use a token indexing
for referencing source code. For instance, in
the snippet `PUSH int 3`; the `PUSH int 3`
instruction will be token #1, `int` #2, curly
and `3` the third token. While efficient, this
sometimes proves impractical. The server is
meant to be used by clients which need to display
information to their users about the code in
terms of line and columns, not tokens.

Michelson's parser main function returns a
`parsing_result`. It is comprised of various
elements.

* The source code which produced this result
* An unexpanded, but linted form of the parsed code
* An expansion table, which links token indices
  to a full location (line, column and character
  offset for the start and end of the token).
* An unexpansion table, which links instructions
  created by macro expansion to their original
  instruction. For instance, take the following
  Michelson code, in which `DUUP` will be
  expanded as `DIP { DUP }; SWAP`.

```michelson
code {
    PUSH int 0;
    PUSH bool True;
    DUUP;
}
```
![Michelson code featuring the DUUP macro](img/blank.png){#fig:duupmacro}

@fig:exptable details the expansion and
unexpansion table stemming from the parsing of
the code present in @fig:duupmacro.

![Expansion table & unexpansion table](graph/expansion_table.dot.png){#fig:exptable width=80%}

### The token problem

The typemap object has the following shape:

```ocaml
type typemap = {
    location: int;
    before: type_stack;
    after: type_stack
} list
```

A typemap is a list detailing the typing of
every stack attained through the program. The
client needs to be able to show the user how the
stack is typed at specific points of their
choosing. This form is too restrictive for that,
as the location is expressed in term of tokens.
Fortunately, the ast object gained from the
parser contains an expansion table from which we
can extract a code location.
 accomplish anything chain-related.

## Architecture

The server is divided in three main modules,
`Server`, `Driver` and `Michelson_v1_json`. For
practical reasons, the `Models` module also
exists in order to share data structures between
the main modules.

### Server

`Server`, which defines the following services (RPC
endpoints):

* `hello`, which is for liveness checks.
* `expand`, to expand a Michelson expression
* `indent`, for linting purposes
* `typecheck`, which typechecks a contract
* `get_trace`, which traces the execution of a
  operation chain.

The `Server` module is simply an interface for
requests. It calls `Driver` for any
computational task.

### Driver

This is the main module. It creates a
context according the parameters given by
`Server`. It calls `Michelson_v1_json`
before returning.

### Michelson_v1_json {#sec:michjson}

`Michelson_v1_json` transforms the OCaml data
structures into usable json data using
data-encoding.

### Typechecking sequence

Figure @fig:typeseq details the call
sequence when the server receives a typecheck
request.

![Typecheck request sequence](out/update_text/typecheck_sequence.png){#fig:typeseq}

When the server receives a typecheck request
from a client, it starts by decrypting the
request (Converting from Json to OCaml
structures). This is done using the Data
Encoding library. A typecheck request is made
of two fields: the code of the contract, and its
storage. It then calls `Driver`'s `typecheck`
function with the decrypted code and storage.
In turn, the `Driver` parses the code to get a
parsing result, described in [@sec:parsing]. Once this is done, the module creates
a new context from scratch, using the steps
described in [@sec:ctxt]. It then calls the
protocol's parser to check if the storage's
value is compatible with the storage type
enunciated in the code. In turn, it calls the
protocol's typechecker in order to get a type map.
Finally, it calls `Michelson_v1_json` to
transform the typemap in Json, before returning
it back to the server.



## Run

One of the main features of Try Michelson is to allow users to
run their contract step-by-step, and inspect the *values* on
the stack at each point.

The trace function is a primitive of the tezos
library, with the following signature.

`context -> ~source: contract -> ~payer: contract -> self(contract, script) -> ~parameter -> ~amount: tez -> Error_monad(trace)`


This function takes several parameters.

* A context, obtained through the initialization
  of a local chain
* The source contract - the identifier of the caller
* The payer - the contract that pays the fees.
  (Those two can be given a bootstrap account
  address).
* Information about the called
  contract itself. Its address - which we need
  for instructions such as `SELF` and `ADDRESS`
  - and its script - its code. 
* A call parameter - which type
  must match the one given in the script - as
  well as the amount transferred to the called
  contract. This information has to be given by
  the user. 

The functions returns a wrapped trace object of
the following shape:

```ocaml
type trace = {
    location: int;
    gas_left: int;
    stack: {item: string} list;
}
```

As explained in [@sec:michseq], there's two
types of Michelson instructions: _primitive_
instructions and _macro_ instructions. One of
the features of the proposed client, needs to
highlight whichever instruction is being ran to
produce a given stack during step-by-step
execution. However, in the case of macros, we
don't want to show the inner instructions (`DIP
{ DUP}; SWAP`), just the unexpanded macro
(`DUUP` in our example). The parsing result
object features an unexpansion table, which
links a token id to its origin id. Our algorithm
proceeds in the following way.


1. Find where a given token stems from using
   the unexpansion table. It will be itself if
   not part of a macro.
2. Get its location through the expansion table.
   1. If none is found, give it a special value -
      we chose zero.
   1. If some is found, check its dependencies.
      Dependencies are obtained through the
      expansion table. 
3. If the token matches the last element of that
   list, it means it was present in the
   unexpanded code. It points to either a simple
   instruction or the whole macro, and is marked
   as such.

```ocaml
let location_from_token(token) ->
  let unexpanded_token = unexpansion_table[token]
  let location, deps = expansion_table[unexpanded_token]
  if location == null then
    return 0 (* The token could not be found *)
  else
    let last = deps[len(deps - 1)]
    if last === token
        return location, false (* The token was in the unexpanded code *)
    else
        return location, true (* The token comes from an expansion *)
```

While this method works for simple contracts, it
is deeply unsatisfactory. It is not
possible to specify the balance of the called
contract. More importantly, the typechecking
fails if the contract refers to another contract
in its source code. These shortcomings prevents
us from being able to simulate a multi-contract
call chain - which is a significant feature.

### A more complete approach

Instead of merely throwing away the context
after each call, we must forward it to the next
operation if one exists. We also have to
consider all the operations that a contract can
emit - `Reveal`, `Transaction`, `Origination`,
and `Delegation` - and the balance of all
participants in the operation chain.

The user must now provide every known contract -
made of address, code, storage and balance - the
balance of each bootstrap account, as well as
the transaction parameters - parameter, gas,
amount, called address, payer.


One of this project's initial goals is not to
re-code what's already in the codebase. Tezos'
codebase features the apply module, in which
those concerns have been taken care of.
We want the user to be able to run through their
code step-by-step, and the apply module doesn't
allow that. We can find that in the
middle of the file, the code calls
`Script_interpreter.execute`, which executes the
code without providing an execution trace. We
could amend that, but that's code present in the
protocol, which means that we would need to wait
for the next amendment to propose the change,
and it would need to be voted in. Thus, we
decided on the simplest solution. We duplicated
the `apply.ml` file, applied the changes -
namely adding tracing (which implies to rewrite
all the data structures present in the original
GADT) - and added it in a single commit. While
this works, the developer now has to manually
merge the apply file. However, for the `mainnet`
branch, this only has to be done roughly every 6
months.

### Contract origination

Another challenge is to have originated all the
contracts before executing the chosen one. While
we could generate origination operations for
each contract to apply to the initial context,
we would have to notify the user at which
address each contract was originated, which
would prove frictionful. Tezos' codebase
features a mechanism similar to the bootstrap
accounts, bootstrap contracts, which is used
by shaping the contract objects in the right
shape. This approach has the interesting
particularity that each contract will always be
originated at the same address based on the
order of origination. This allows us to bypass
the back and forth that would have happened
using the operation method.

Figure @fig:applyact details how the apply
algorithm proceeds.

![Apply algorithm operation](out/update_text/apply_loop.png){#fig:applyact width=33%}




