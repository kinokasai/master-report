## The editor

We've decided to use the package `react-monaco-editor` as to
not wrap it ourselves. However, `react-monaco-editor` is a
JavaScript package, so we have to write typing bindings.

Fortunately, Reason and ReasonReact introduce a lot of
helpers to alleviate the pain of writing those. For example,
we can get access to the a JavaScript class through `@bs.module`.

### A Typescript interlude

Monaco is written in Typescript, which means the
easier and safer way to use it is through
Typescript. Based on this observation and advice
by the creator of `sketch` @sketch, we decided
to write most of the editor logic in Typescript,
and bind those functions to Reason later on.
This has the benefit of massively reducing the
surface for which to write bindings. Monaco is
also undergoing rapid development making its API
change often, which makes it a bit of a moving
target. Fortunately, as both Typescript and
Reason transpile to JavaScript, interoperability
is possible between the two.

### Stack printing

In order to achieve feature parity with emacs'
michelson-mode, we need to be able to show the
user typing information corresponding to its
cursor position. While its fairly easy to get
access to the cursor position in Typescript
land, it's not in Reason land. Thus, we created
a Typescript function `onCursorMove`, that takes
a callback. When the editor is initialized in
Reason, we call that function with a `dispatch`
method, which is a reducer function changing the
type stack value in the Reason state.

### Hover

One of Try-michelson advantages over its emacs
companion is the hover feature. When an user
hovers over an instruction, a tooltip containing
documentation is displayed. This uses monaco's
`hoverProvider` API. The documentation has be
leveraged from the `rst` file used in the online
documentation [@tezosdoc].

### Completion

Try-michelson also allows for completion. This uses monaco's
`completionItemProvider` API. It finds what could be
meaningful suggestions in the context, and lets monaco do
the filtering. Fortunately, Michelson is both a concise and
elegantly devised language, so there is not much place for
confusion. Everything that follows a `{` or `;` and starts
with an uppercase letter must be a primitive. Similarly,
everything that follows a primitive and starts with a
lowercase letter must be a type.

### Syntax Highlighting

Finally, syntax highlighting is provided through monaco's
`monarchTokensProvider`. Its usage is quite simple as it's
like building a small lexer. The user just has to provide
regexps and ids corresponding to that regex.

[Listing @lst:regex] details the supplied regular expressions.

```{#lst:regex .typescript caption="Token regexes"}
[/[0-9]+/, "number"]
[/[A-Z][A-Z]+/, "primitive"]
[/[A-Z][a-z]+/, "constant"]
[/\"[^\"]*\"/, "string"]
[/#.*$/, "comment"]
[/(code|parameter|storage)/, "section"]
[/[a-z]+/, "type"]
```


