# Introduction

## Blockchain

Blockchains can be seen as an immutable database
operating in a decentralized network. They are
built upon several key concepts and tools:

* They heavily use cryptography to ensure users
  authentication as well as the database
  immutability
* They offer a probabilistic solution to the
  "Byzantine generals problem" [@byzantine] for
  consensus among all participants - which we
  will call nodes in the rest of this document -
  in the decentralized network
* They use a peer to peer gossip network for
  low-level communications between the nodes.

Blockchains are often called _crypto-ledgers_ as
they can be seen as an electronic book,
recording transactions where users' identity and
book immutability are cryptographically ensured.

In order to validate and append transactions to
the ledger, all blockchains follow a similar
generic algorithm. A transaction is a token
transfer from an account to an account (it can
be itself).

1. New transactions are broadcast to all nodes
   which aggregate them in blocks.
2. The next block is broadcast by one or several 
   nodes.
3. Nodes express their acceptance of a block by
   including its cryptographic hash in the next
   block they create.

In abstract terms, a Blockchain is built upon
two functions, `apply` and `score`. `apply`
takes the current state and a block to produce a
new state. `score` computes the score of a state
to choose the preferred one between multiple
states.

Three main properties of the blockchain are
_liveness_, _finality_ and _safety_.

Liveness ensures that progress is possible. In
terms of consensus, it is the guarantee that all
actors will eventually agree on a value.

Safety is the guarantee that nothing bad will
ever happen. In a consensus mechanism, this
means that no two actors ever decide on
different values.

Finality is the guarantee that a block will not
be revoked and that past transaction can never
change.

Blockchains face some common distributed systems
challenges. To be resilient to Sybil attacks
[@sybil] a solution is to restrict the pool of
block producers by tying it to the use of a
scarce resource. A difficulty is to choose this
resource and create incentives that push the
majority of the network to be honest.
Restricting the pool of block producers can also
lead to liveness issues that have to be taken
into account to make sure that the chain does
not stop whenever a block producer is offline.
Blockchains also have to consider malicious
block producers and performance issues such as
network delays. Each blockchain provides its own
set of solutions to overcome these challenges
but most of them rely on the foundations set by
Bitcoin [@nakamoto2008bitcoin]. Blockchains also
face the risk of forks: to update their economic
protocol, blockchains have to go through social
consensus and risk frictions in their user
community that may lead to the birth of hard
forks splitting the community in two parts
agreeing on two different chains. Such hard fork
occurred after the Ethereum DAO hack [@daohack]
leading to the birth of two blockchains:
Ethereum Classic and Ethereum.

### Bitcoin

Bitcoin was introduced in 2008. The main
objective behind Bitcoin was to propose a
decentralized electronic cash system. Bitcoin is
the name of the blockchain as well as the name
of its cryptocurrency. Blockchains use tokens to
represent assets stored in the chain. Some of
these tokens, as in Bitcoin, can represent a
currency. Bitcoin is also a decentralized system
(the actual blockchain) allowing users to store
and transfer their tokens. This system is the
combination of peer-to-peer (P2P) network
associated with a consensus protocol to maintain
consistency between nodes. This consensus
algorithm introduced the notion of
_Proof-of-work_ (PoW). With PoW, the scarce
resource used to restrict the pool of block
producers - which Bitcoin calls "miners" - is
computing power. High computing power mostly
demands very efficient computing hardware
associated with high energy consumption. The
main idea is to request that miners compete in
the solving of a puzzle to earn the right to
produce (mine) the next block. With PoW, Sybil
attacks are difficult and expensive. Associated
with incentives to motivate miners to compete,
liveness is easy to achieve. In case of _soft
forks_ (temporary split) of the chain, which can
be caused by bugs, network latency or malicious
mining, and to maintain consistency of the chain
among all the nodes, each node has to decide
which branch is likely to be the winning chain.
This is done by computing the _score_. The node
starts from a tree of branches, and at each
iteration, it looks at the score of all branches
and picks the winner. Bitcoin’s consensus
algorithm specifies to always keep the longest
chain between two forks. A chain is considered
longer if its total difficulty (summing the
difficulty of the puzzles solved to mine each
block of the chain) is higher.

The difficulty is adapted every two weeks to
maintain an average of 10-minute intervals
between two consecutive blocks depending on the
global computing power of the miners. PoW is the
solution currently used by most blockchains.


### Decentralized platforms

While Bitcoin focused on electronic cash, it
also comes with the concept of decentralized
computations: Bitcoin's script allows many
interesting forms of _in-transaction_
computation, and others quickly proposed to use
blockchains to build decentralized computing
platforms [@buterin2014ethereum]. This was popularized by the current
second-biggest blockchain, Ethereum, in 2014,
which pushed the concept further. The main idea
is to see blockchains as vending machines where
users can pay for a service. In blockchain
platforms, these services are small programs
living on the chain. Users of the blockchain can
store code in blocks and other users can execute
this code. These programs are called _Smart
Contracts_. They can, of course, perform
blockchain operations, such as token transfers,
but they can also be used for access control or
to interact with others. Some smart contracts,
being fed with off-chain information, serve as
_Oracles_ selling trusted information. Smart
contracts can be used in many applications such
as financial contracts, developing new
currencies on top of the basic crypto-currency
of the blockchain, such as voting systems,
games, or crowdfunding.

## Tezos specifics

The Tezos blockchain was presented in 2014 and
released in 2018. Contrarily to most new
blockchains at the time of its presentation, it
not based on PoW.

### Proof-of-stake

The current Tezos protocol is based on a _Liquid
Proof-of-Stake_ (LPoS) consensus algorithm.

PoS is very different from PoW. It considers the
stake - the number of tokens - that users hold
as the primary resource used to build the pool
of block producers - called _bakers_ in the
Tezos ecosystem. In the current Tezos consensus
protocol, to push a block at a certain level,
bakers are randomly selected using a lazy
infinite priority list of baking slots. In order
to participate in this random selection, a baker
must hold at least a roll of tokens -
corresponding to 8000 tokens at the time of this
writing. The number of baking slots is
proportional to the number of rolls that a baker
holds. However, participants that do not hold
enough tokens or who do not wish to bake blocks
can delegate their tokens to another baker, much
like in Liquid Democracy one can delegate their
right to vote. They keep the ownership of their
tokens but increase the stake of their delegate
in the random assignment of baking slots.
Delegation makes the PoS system fairer and
participative and helps balance a possible
concentration of tokens in a few hands.

Finality is the guarantee that a block will not
be revoked and that past transaction can never
change.
In order to help the chain reach finality faster,
 Tezos' PoS mechanism introduces
endorsements of
baked blocks. For each baked block, 32
endorsements - signatures - slots are created,
allowing chosen bakers to approve a block by
signing it. Using endorsements, the highest
block resulting score is considered the head of
the chain where the score is:

`score(block_n + 1) = 1 + score(block_n) + nb_endorsements`

In order to provide incentives to bakers for
participating in the network, the protocol
rewards baking and endorsing.

Tezos' LPoS consensus algorithm, _via_ its
internal mechanism and its associated
incentives, solves the challenges of the
platform without requiring significant
computational power. It also focuses on the
users of the platform instead of external actors
as it is possible with PoW: only stake-holders
can participate but all can delegate their
stake.

### Self-amending blockchain

Tezos is a self-amending blockchain. The
protocol that validates blocks and implements
the consensus algorithm can amend itself.
Concretely, a new protocol is downloaded from
the network, compiled, and hot-swapped to
replace the current one.

The amendment procedure can be triggered in
several ways, depending on the protocol. In the
current Tezos economic protocol, an amendment
can only be triggered as the result of an
on-chain voting procedure.

It helps avoiding forks of the chain and reduces
friction in the community. A protocol amendment
may consist of very important upgrades such as a
switch to a completely different consensus
protocol. It can also consist in smaller
modifications such as extending the smart
contract language, modifying the rewarding
system to enforce network participation or
adding new kinds of transactions, for instance
anonymous ones.

## Tezos as a decentralized platform

Tezos' economic protocol not only handles a
registry of transactions, but also has support
for smart contracts.

Smart contracts are small programs registered in
the blockchain together with a private data
storage, but the data is publicly visible. A
contract registered in the chain is said to be
_originated_ and has an address prefixed by
`KT1`, which is given in the contract
origination's block.

They are executed by performing specific
transactions to their associated account (their
address). The transaction carries data that is
passed as a program parameter and can thus be
viewed as a procedure call.

The execution of a smart contract can change the
state of its storage and trigger on-chain
operations. A contract can call another contract
by this mechanism. Such operations are called
_internal operations_.

Smart contract languages are usually
Turing-complete. However, the replicated nature
of the contract storage and the liveliness
requirement of the consensus algorithm imposes
some restrictions on their execution.

### Limited execution time

Any call to a smart contract, once included in a
block, will be executed on every node in the P2P
network, as they have to validate the block
before including it in their view of the chain
and before passing it to their neighbors. This
means that the execution time of each smart
contract call included in a block has to fit
multiple times in the inter-block time of the
chain to ensure its liveness.

In order to restrict the computation cycles
allocated to each smart contract execution, each
instruction is associated to a _gas_ unit,
crafted to be proportional to the actual
execution time, and the smart contract to the
maximum number of gas units allowed to burn. The
interpreter running the smart contract burns an
amount of gas which is crafted to be
proportional to the actual execution time. If an
execution exceeds its allowed gas consumption,
it is stopped immediately and the effects of the
execution are rolled back. The transaction is
still included in the block and the fees are
taken, to prevent the nodes from being spammed
with failing transactions.

In Tezos, the economic protocol sets the gas
limit per block and for each transaction, and
the emitter of the transaction also sets an
upper bound for its transaction's gas
consumption. Each baker can set their own
strategy for accepting or refusing a block. The
default strategy requires the transaction fee to
be proportional to the gas upper bound.

## Michelson

Michelson is the language in which the smart
contract scripts are written. The Michelson
language has been designed before the launch of
the Tezos blockchain. The most important parts
of Michelson's implementation, the typechecker
and the interpreter, belong to the economic
ruleset of Tezos - the protocol - so that the
language can evolve through the Tezos amendment
voting process.

### Design Rationale

Smart contracts operate in a very constrained
context: they need to be expressive, evaluated
efficiently, and their resource consumption must
be accurately measured as their execution time
impacts block construction and propagation.
Smart contracts are non-updatable programs that
can handle valuable assets, there is thus a need
for strong guaranties on the correctness of
these programs.

The need for efficiency and more importantly for
accurate account of resource consumption leans
toward a low-level interpreted language, while
the need for contract correctness leans toward a
high level, easily auditable, easily
formalisable language, with strong static
guaranties.

To satisfy these constraints, Michelson was made
a Turing-complete, low level, stack based
interpreted language - not unlike _Forth_ -
enabling resource measurement, but with some
high-level ML-like features: polymorphic
products, options, sums, lists, sets, and maps
data-structures with collection iterators,
cryptographic primitives and anonymous
functions. Instructions are pure functions that
take a stack as input and return a stack as
output.

### Shape {#sec:michshape}

A Michelson contract script is written in three
parts: the parameter type, the storage type, and
the code of the contract. The code consists in
one block of code that can only be called with
one parameter, but multiple entry points can be
encoded by branching on a nesting of sum types.

When the contract is originated on the chain, it
is bundled with a data storage which can then
only be changed by the successful execution of a
contract. The parameter and the contract's
associated storage are paired and passed to the
contract's code at each execution, at the end of
which it has to return a list of operations and
the updated storage.

As usual in stack-based languages, Michelson
code consist of a sequence of instructions. Each
instruction takes its parameters on the stack,
and put their result back on the stack. Each
instruction encodes a stack transformation.
Instructions can be divided in three types:
_simple_, _nesting_, and _macros_. Simple
instructions are basic, like `DUP`, which
duplicates the top value of the stack. Nesting
instructions can be parameterized by other
instructions. That's the case of `DIP`. In `DIP
{ DUP }`, the body of the `DIP` instruction is
executed on the second value of the stack.
Macros are not primitive instructions. They are
expanded to a sequence of primitive
instructions. `DUUP` is a macro that expands to
`DIP { DUP }; SWAP`.



### A short example

In @lst:meaningless, we present a
simple example, annotated by indices
denoting each stack change. The following graph
shows the state of the stack at each annotated
point.

```{#lst:meaningless .michelson caption="A simple example"}
parameter unit;
storage unit;
code { (0)
  PUSH int 2 ; (1)
  PUSH bool True ; (2)
  LOOP { (3)
    PUSH int 1 ; (4)
    SWAP ; (5)
    SUB ; (6)
    DUP ; (7)
    GT (8)
  } ;
  DROP ; (9)
}
```

0. The contract loads its storage and parameter
1. `2` is pushed onto the stack
2. `True` is pushed onto the stack
3. The code starts to loop. `LOOP` takes a
   boolean as parameter.
4. `1` is pushed onto the stack
5. The first and second elements of the stack
   are swapped
6. The second element is subbed from the first
7. The top element is duplicated
8. `GT` pushes `True` if the top element is
   greater than 0, `False` else
9. `DROP` pops the first value of the stack.


![Stack evolution](graph/meaningless_stack_evo.dot.png){#fig:stackevo}

It can be difficult for an human programmer to
keep the stack in mind, particularly so with
complex contracts. A stack-based language is
easier to reason about, but is also is a
compromise as it then becomes more difficult for
programmers.

## Interacting with the Tezos blockchain

The binary architecture is centered around two
main components.

Firstly, the _node_ is responsible for
connection to peers through the gossip network
and updating the ledger's state - also called
_context_. As all the blocks and operations are
exchanged between nodes on the network, the node
is able to filter and propagate data from/to its
connected peers. Using the blocks received on
the gossip network, the nodes keeps an
up-to-date context.

Secondly, the _client_ is the main tool to
interact with a Tezos blockchain node.

There are currently three public Tezos networks:

* `mainnet`, the current incarnation of the
  Tezos blockchain. It runs with real tez (Tezos
  tokens) that have been baked or allocated to
  the donors of 2017's fundraiser. It has been
  live since June 30th 2018.
* `alphanet`, which is based in the `mainnet`
  codebase but uses free tokens. It is the
  reference network for developers wanting to
  test their software.
* `zeronet` which is the testing network, with
  free tokens and frequent resets.

It is also possible to run a "localhost-only"
network, through sandboxed node and client.

### Sandbox node

When a new network is bootstrapped, the network
is initialized with a dummy economic protocol,
called _genesis_. It is possible to run the same
protocol as alphanet, by launching the
`tezos-activate-alpha` command, which will
inject the current protocol.



### Tezos' architecture (in the large)

![Tezos' octopus](img/octopus.svg){#fig:octopus}

The characteristic that makes Tezos unique is
its self-amending property. The part that amends
itself is called the _economic protocol_,
sometimes abbreviated by protocol. 


The protocol only sees one blockchain - a linear
sequence of blocks since the genesis (the first
block). It does not know that lives in an open
network where nodes can propose alternative heads.

The rest of a Tezos node is called the _shell_.
Only the shell knows about the multiple heads.
It is responsible for choosing between the
various chain proposals coming from the bakers.
The shell has the responsibility of selecting
and downloading alternative chains, feed them to
the protocol, which in turn has the
responsibility to check them for errors, and
give them an absolute score. The shell then
simply selects the valid head of highest
absolute score. This part of the shell is called
the validator.

The rest of the shell includes the peer-to-peer
layer, the disk storage of blocks, the
operations to allow the node to transmit the
chain data to new nodes and the versioned state
of the ledger. In-between the validator, the
peer-to-peer layer and the storage sits a
component called the distributed database, that
abstracts the fetching and replication of new
chain data to the validator.

Protocols are compiled using a tweaked OCaml
compiler (green part on the left of the picture)
that does two things. First, it checks that the
protocol’s main module has the right type. A
good analogy is to see protocol as plug-ins, and
in this case, it means that it respects the
common plugin interface. Then, it restricts the
typing environment of the protocol’s code so
that it only calls authorized modules and
functions. Seeing protocols as plug-ins, it
means that the code only called primitives from
the plug-in API. It is a form of statically
enforced sandboxing.

Finally, the RPC layer (in yellow on the right
in the picture) is an important part of the
node. It is how the client, third party
applications and daemons can interact with the
node and introspect its state. This component
uses the mainstream JSON format and HTTP
protocol. It uses in-house libraries
ocplib-resto and ocplib-json-typed (via the
module Data_encoding). It is fully
inter-operable, and auto descriptive, using JSON
schema.

## The protocol

The in-work protocol in called protocol Alpha.
Protocol Alpha, whose Alpha has nothing to do
with the one in Alphanet, is the name of the
initial economic protocol. Alpha is a
placeholder name, while is decided the naming
convention for protocol versions.

As all protocols, Alpha is made of a series of
Ocaml files.

### Alpha context

The Proof-of-Stake algorithm, as described in
the whitepaper [@whitepaper], relies on an
abstract state of the ledger, that is read and
transformed during validation of a block.

Due to the polymorphic nature of Tezos (the
protocol is subject to amendments), the ledger's
state - called *context* in the code - cannot be
specific to protocol Alpha's needs. The PoS is
thus implemented over a generic key-value store
whose keys and associated binary data must
implement the abstract structure.

### Protocol modules

The protocol notably implements the
`Script_interpreter` and `Script_ir_translator`,
which are required to parse and run Michelson
programs.

### Client modules

Helper modules such as `Michelson_v1_parser` and
`Michelson_v1_error_reporter` are implemented
inside `lib_client`, that is to say outside of
the protocol.

## Contribution

As of February 2019, the only way to get
assistance in the writing of a Michelson smart
contract is to use an emacs plugin named
michelson-mode. It connects to a full-blown
Tezos node in order to retrieve typing
information.

This simplistic approach was both too
restrictive and too heavy. Tezos is a relatively
young project, whose future success is linked to
adoption. People who (will) write smart
contracts aren't necessarily developers at
heart. Complicated tools such as emacs and
relative unfamiliarity with Tezos' software
ecosystem - launching your first node is
non-trivial - could hinder such adoption.

The proposed contribution has three goals: Not
embedding more than necessary, not forcing any
particular editor on the user, and reusing as
much code as possible from the node.

## Outline

This project is divided in two parts.

1. A server, built on top of the protocol, which
   exposes remote procedure calls for Michelson
   development helpers, such as typechecking and
   execution of Michelson smart contracts.
2. A web client, exposing an editor with modern
   features, such as syntax highlighting,
   linting, on-the-fly typechecking, completion,
   and step-by-step debugging.

