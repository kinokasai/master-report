## Stack Component

The stack component is an example of a stateless
component. Its only function is to display a
graphically correct depiction of a stack, which
is given as a prop.

In Try-michelson, each stack is displayed next
to another stack, either the stack before the
instruction is executed, or after (depending on
which one it is). It means that each stack must
be bottom-aligned, hence the `max_stack_size`
prop.

Then, values in Michelson can be arbitrarily
large - Pairs, long addresses, hashes... We
decided to limit the max width of a stack.
Through experimentation, we settled on 32
characters max per row, as well as centered
text.

[Listing @lst:padfun] details the pad function
used to center the text.

```{#lst:padfun .reason caption="The pad function"}
let pad = (max_width, s) => {
  let len = String.length(s);
  if (len > max_width) {
    String.sub(s, 0, max_width - 3) ++ "...";
  } else {
    let space = (max_width - len) / 2;
    String.make(space, ' ') ++ s ++ String.make(space + 1, ' ');
  };
};
```

\include stack.png
