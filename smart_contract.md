# Anatomy of a smart contract

In this section, we'll use the term `protocol`
interchangeably to mean both the economic protocol and
`protocol alpha`, which is the current instance of economic
protocol.

## Accounts

In the protocol, an account is an unique identifier. The
context associates each account with a balance - how much
money it has access to - a manager - another account that
manages this one, possibly itself. The identifier, also
called an address, can take different shapes, in the form of
prefixes. For instance, an account starting with `tz1` will
be a simple account whereas one starting with `KT1` will be
a contract, that is to say an account associated with a
Michelson script.

## Contracts

A smart contract is therefore comprised of four elements.
Its address - `KT1...`, its balance, but also its storage -
what state will it remember between calls - as well as its
code, which will be ran every time the contract is called.

