doc:
	(cd graph; dot -Tpng -O *.dot)
	bash include_md.sh report.md > full_report.md;\
	./side_code.sh full_report.md;\
	pandoc -t json out.md | python ./tex_templates/underline.py | \
	pandoc --csl ieee.csl --filter pandoc-crossref  --bibliography biblio.yaml --bibliography blockchain.yaml --filter pandoc-citeproc -f json -o report.pdf --pdf-engine=xelatex --toc \
	--template=tex_templates/template.latex &&\
	echo "Done."

preview: doc
	zathura report.pdf

clean:
	rm full_report.md report.pdf out.md