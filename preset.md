## Presets

Try-michelson has a pedagogical aspect to it. In that light,
we've decided to develop a preset system. Presets are stored
in json form, under the aptly-named file `presets.json`. It
allows other teams, such as the Michelson team to add or
alter those presets without touching the code.

There's two types of presets. Contract presets, and
scenarios.

### Contract presets

Contract presets, like their name suggest, describe
contracts. Basically, it's comprised of three of the four
contract components - the code, the storage and the balance - plus a name. This serves several purposes. Firstly, the
presets are presented through the dropcard component
presented above. Secondly, for a given scenario, one might
want to reference an already-described contract.

### Scenario

Scenarios encompass more than their contract counterpart.
Indeed, not only they can be comprised of several contracts,
they also have to express the balance of each bootstrap
account, as well as the parameter of the leading transaction.