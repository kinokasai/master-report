# Client

## Technological choices

Try-michelson is mostly written in ReasonML.
Small parts have been coded in Typescript.
Several factors have influenced this decision.
While having no experience in Reason, Nomadic
Labs is primarily an OCaml shop. Reason is
proving to be a more pleasant and robust way to
build web applications using react, leveraging
OCaml's type system - it can interesting to note
that react was initially prototyped in SML.

As for the text editor, our choice fell on the
increasingly popular editor, monaco. Its
development speed as well as exposing a lot of
features while keeping usage relatively simple
led it to be the de facto editor in web space.

Keeping the UI in sync with the state is
non-trivial. That's why a number of JavaScript
library have started popping in the last few
years - such as Angular, Vue, or React.

React is a declarative and efficient JavaScript
library for building user interfaces. It lets
you compose complex UIs from small and isolated
pieces of code called “components”. Recently, a
Facebook team started work on a wrapper for
reactjs called _ReasonReact_. It builds upon the
foundation laid by reactjs, but is a ReasonML
library. Reason is a perfect fit for React, as
its core principles - immutability,
functional programming and type system - are
built into Reason itself.

## Components

### A react primer

In ReasonReact, we can differentiate a number of components.
The most basic ones - called stateless - are purely
graphical. However, each component can be instantiated by a
number of elements, called _props_. For example, a message
component may take a prop of type `string`, the message
text.

A more interesting and usual component is the *reducer*. In
addition to its graphical part, it possesses both _state_, and
a way to act on it, _actions_. It applies the actions
through a `reducer` or `dispatch` method, which is often
implemented as a pattern matching on the state|action tuple.

In this report, we'll describe the react components
implemented during the course of this project by describing
their state and actions, then showing the result in picture
form.

### About bindings

As ReasonML is a statically typed language,
typing operability with JavaScript comes with a
cost. If wanting to use a JavaScript library
while developing in Reason, one must beforehand
write type bindings for each function one wants
to call. While a bit cumbersome, a Reason
application can have access to whole JavaScript
ecosystem.

## Architecture

![Component architecture](out/update_text/architecture.png)

![Try Michelson UI](img/ui-try-michelson.png){width=90%}

The Controller component is the main component of the
application. It embeds four other sub
components.

* `History` shows the operation chain stemming
  from the first transaction. 
* `ParamsForm` is the form in which the user
  specifies the context in which the contracts
  will be run. 
* `StackComponent` prints the
  stacks when the user is tracing a contract's
  execution.
* The `Contract` component is host to the editor
  itself, and contains everything contract
  related. There can be several `Contract`
  components displayed at the same time. Let us
  now see how they interact through some
  scenarios.

### Submit

![Submit sequence diagram](out/update_text/submit_sequence.png){#fig:submitseq width=75%}

In @fig:submitseq, we show what happens when an
user submits a transaction.

When an user clicks on the submit button in the
`ParamsForm`, the form calls it submit callback given by the
`Controller`. The `Controller` then emits a trace request to
the server, which responds with an history. In turn, the
`Controller` loads that history, builds any zipper tree that
may exist, and changes its internal state, which is
reflected to the user in the form of an history inspecting
interface.